const functions = require('firebase-functions')
const admin = require('firebase-admin')
admin.initializeApp()

const db = admin.firestore()

module.exports = {
    ...require('./src/team'),
    ...require('./src/member')
}