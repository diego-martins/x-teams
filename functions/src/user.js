const functions = require('firebase-functions')
const admin = require('firebase-admin')
const { tsToMillis, normalize } = require('./utils.js')
const config = require('./config')
const db = admin.firestore()

exports.onCreateUser = functions.firestore
    .document('users/{userId}')
    .onCreate(async (snap, context) => {
        const { userId } = context.params
        const data = snap.data()
        //normalize name
        const name = `${data.firstName} ${data.lastName}`
        snap.ref.set({ lowerName: normalize(name) }, { merge: true })
    })

exports.onUpdateUser = functions.firestore
    .document('users/{userId}')
    .onUpdate(async (change, context) => {
        const { userId } = context.params
        const data = change.after.data()
        const prevData = change.before.data()

        //normalize name
        const name = `${data.firstName} ${data.lastName}`
        const prevName = `${prevData.firstName} ${prevData.lastName}`
        if (name !== prevName) {
            await change.after.ref.set({ lowerName: normalize(name) }, { merge: true })
        }

        if (data.emotion === prevData.emotion &&
            data.attention === prevData.attention) return null

        if (!data.deletedAt) {
            await addTeamLog(teamId, data)
        }

        return true
    })
