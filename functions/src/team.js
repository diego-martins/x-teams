const functions = require('firebase-functions')
const admin = require('firebase-admin')
const { tsToMillis, normalize } = require('./utils.js')
const config = require('./config')
const db = admin.firestore()

const addTeamLog = async (teamId, data) => {
    const timestamp = Date.now()
    const doc = {
        modifiedAt: admin.firestore.Timestamp.fromMillis(timestamp),
        emotion: data.emotion,
        attention: data.attention
    }

    // get last log to compare interval
    const lastSnaps = await db.collection('teams').doc(teamId)
        .collection('teamLogs').orderBy('modifiedAt', 'desc')
        .limit(1).get()

    if (!lastSnaps.empty) {
        // update if interval is less than X time
        let lastDoc
        lastSnaps.forEach(doc => {
            lastDoc = {
                id: doc.id,
                ...doc.data()
            }
        })

        const lastInterval = timestamp - tsToMillis(lastDoc.modifiedAt)
        if (lastInterval < config.logInterval) {
            await db.collection('teams').doc(teamId)
                .collection('teamLogs').doc(lastDoc.id)
                .update(doc)

            return true
        }
    }

    doc.createdAt = admin.firestore.Timestamp.fromMillis(timestamp)
    doc.teamId = teamId

    await db.collection('teams').doc(teamId)
        .collection('teamLogs').doc()
        .set(doc)

    return true
}

exports.onCreateTeam = functions.firestore
    .document('teams/{teamId}')
    .onCreate(async (snap, context) => {
        const { teamId } = context.params
        const data = snap.data()
        await snap.ref.set({
            lowerName: normalize(data.name),
            lowerCompany: normalize(data.company)
        }, { merge: true })
        return db.collection('planningPoker').doc(teamId)
            .set({
                cardSequence: 'standard'
            })
    })

exports.onUpdateTeam = functions.firestore
    .document('teams/{teamId}')
    .onUpdate(async (change, context) => {
        const { teamId } = context.params
        const data = change.after.data()
        const prevData = change.before.data()

        if (data.name !== prevData.name || data.company !== prevData.company) {
            await change.after.ref.set({
                lowerName: normalize(data.name),
                lowerCompany: normalize(data.company)
            }, { merge: true })
        }

        if (data.emotion === prevData.emotion &&
            data.attention === prevData.attention) return null

        if (!data.deletedAt) {
            await addTeamLog(teamId, data)
        }

        return true
    })

exports.onDeleteTeam = functions.firestore
    .document('teams/{teamId}')
    .onDelete(async (snap, context) => {
        const { teamId } = context.params
        return db.collection('planningPoker').doc(teamId).delete()
    })    