const admin = require('firebase-admin')

const tsToMillis = (timestamp) => {
    const ts = new admin.firestore.Timestamp(timestamp._seconds, timestamp._nanoseconds)
    return ts.toMillis()
}

const normalize = (str) => {
    if (!str) return ''
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase()
}

module.exports = {
    tsToMillis,
    normalize
}