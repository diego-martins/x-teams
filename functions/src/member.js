const functions = require('firebase-functions')
const admin = require('firebase-admin')
const { tsToMillis, normalize } = require('./utils.js')
const config = require('./config')
const db = admin.firestore()

const addMemberLog = async (teamId, memberId, data) => {
    const timestamp = Date.now()
    const doc = {
        modifiedAt: admin.firestore.Timestamp.fromMillis(timestamp),
        emotion: data.emotion,
        attention: data.attention
    }

    // get last log to compare interval
    const lastSnaps = await db.collection('teams').doc(teamId)
        .collection('teamMembers').doc(memberId)
        .collection('memberLogs').orderBy('modifiedAt', 'desc')
        .limit(1).get()

    if (!lastSnaps.empty) {
        // update if interval is less than X time
        let lastDoc
        lastSnaps.forEach(doc => {
            lastDoc = {
                id: doc.id,
                ...doc.data()
            }
        })

        const lastInterval = timestamp - tsToMillis(lastDoc.modifiedAt)
        if (lastInterval < config.logInterval) {
            await db.collection('teams').doc(teamId)
                .collection('teamMembers').doc(memberId)
                .collection('memberLogs').doc(lastDoc.id)
                .update(doc)

            return true
        }
    }

    doc.createdAt = admin.firestore.Timestamp.fromMillis(timestamp)
    doc.memberId = memberId
    await db.collection('teams').doc(teamId)
        .collection('teamMembers').doc(memberId)
        .collection('memberLogs').doc()
        .set(doc)

    return true
}

const updateTeamAverage = async (teamId) => {
    // get all members of team to calc average
    const docs = await db
        .collection('teams').doc(teamId)
        .collection('teamMembers')
        .where('deletedAt', '==', null).get()

    // counters to average
    let countEmotion = 0
    let countAttention = 0
    let emotion = 0
    let attention = 0

    docs.forEach(doc => {
        const data = doc.data()
        if (data.emotion) {
            countEmotion++
            emotion += data.emotion
        }
        if (data.attention) {
            countAttention++
            attention += data.attention
        }
    })

    await db.collection('teams').doc(teamId)
        .update({
            emotion: countEmotion ? Math.round(emotion / countEmotion) : 0,
            attention: countAttention ? Math.round(attention / countAttention) : 0
        })

    return true
}

exports.onCreateTeamMember = functions.firestore
    .document('teams/{teamId}/teamMembers/{memberId}')
    .onCreate(async (snap, context) => {
        const { teamId, memberId } = context.params
        const data = snap.data()
        await snap.ref.set({
            lowerName: normalize(data.name)
        }, { merge: true })
        await updateTeamAverage(teamId)
        await addMemberLog(teamId, memberId, data)
        return true
    })

exports.onUpdateTeamMember = functions.firestore
    .document('teams/{teamId}/teamMembers/{memberId}')
    .onUpdate(async (change, context) => {
        const { teamId, memberId } = context.params
        const data = change.after.data()
        const prevData = change.before.data()

        if (data.name !== prevData.name) {
            await change.after.ref.set({
                lowerName: normalize(data.name)
            }, { merge: true })
        }

        if (data.emotion === prevData.emotion &&
            data.attention === prevData.attention) return null

        await updateTeamAverage(teamId)
        if (!data.deletedAt) {
            await addMemberLog(teamId, memberId, data)
        }

        return true
    })

exports.onDeleteTeamMember = functions.firestore
    .document('teams/{teamId}/teamMembers/{memberId}')
    .onDelete(async (snap, context) => {
        const { teamId } = context.params
        await updateTeamAverage(teamId)
        return true
    })