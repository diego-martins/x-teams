import React from 'react'
import firebase from './utils/firebaseUtils'

export const useAuth = () => {
    const [state, setState] = React.useState(() => {
        const user = firebase.auth().currentUser
        return { initializing: !user, user }
    })

    function onChange(user) {
        if (user) {
            user.name = user.displayName || ''
            const parts = user.name.split(' ')
            if (parts.length) {
                user.firstName = parts[0]
                if (parts.length > 1) {
                    user.lastName = parts[1]
                }
            }
        }
        setState({ initializing: false, user })
    }

    React.useEffect(() => {
        const unsubscribe = firebase.auth().onAuthStateChanged(onChange)
        return () => unsubscribe()
    }, [])

    return state
}