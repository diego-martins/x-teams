import React from 'react'
import {
  BrowserRouter as Router,
  Switch
} from "react-router-dom"
import PrivateRoute from './components/PrivateRoute'
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'
import { useAuth } from './useAuth'
import theme from './config/theme'
import MyDialog from './components/MyDialog'
import UserContext from './contexts/UserContext'
// import { useTranslation } from 'react-i18next'
// import { CircularProgress, Typography, Grid } from '@material-ui/core'
import loadable from '@loadable/component'
// import pMinDelay from 'p-min-delay'
import './App.css'
import { DialogProvider, DialogConsumer } from './contexts/DialogContext'

// const LoadingPage = (textLoading) => {
//   return (
//     <Grid
//       container
//       spacing={0}
//       direction="column"
//       alignItems="center"
//       justify="center"
//       style={{ minHeight: '100vh' }}
//     >
//       <Grid item style={{ textAlign: 'center' }}>
//         <CircularProgress size={20} style={{ margin: 'auto' }} />
//         <Typography variant="subtitle2" style={{ fontWeight: 500 }} align="center">
//           {textLoading}...
//         </Typography>
//       </Grid>
//     </Grid>
//   )
// }

const App = () => {
  // const { t } = useTranslation()
  // const textLoading = t('loading').toLowerCase()

  const { initializing, user } = useAuth()
  if (initializing) {
    return <div></div>
  }

  // firebase.messaging().onTokenRefresh(() => {
  //   firebase.messaging().getToken().then((refreshedToken) => {
  //     console.log('Token refreshed.')
  //     UserService.updateFCMToken(refreshedToken)
  //   }).catch((err) => {
  //     console.error('Unable to retrieve refreshed token ', err)
  //   })
  // })

  // requestPermission().then(token =>
  //   UserService.updateFCMToken(token)
  // )

  const loadableOptions = {}
  const SignIn = loadable(() => import('./pages/SignIn'), loadableOptions)
  const SignUp = loadable(() => import('./pages/SignUp'), loadableOptions)
  const ForgotPassword = loadable(() => import('./pages/ForgotPassword'), loadableOptions)
  const Teams = loadable(() => import('./pages/Teams'), loadableOptions)
  const TeamDetails = loadable(() => import('./pages/TeamDetails'), loadableOptions)
  const PlanningPoker = loadable(() => import('./pages/PlanningPoker'), loadableOptions)

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <DialogProvider>
        <UserContext.Provider value={{ user }}>
          <Router>
            <Switch>
              <PrivateRoute exact path='/' component={SignIn} redirectAutenticatedTo='/teams' />
              <PrivateRoute path='/sign-in' component={SignIn} redirectAutenticatedTo='/teams' />
              <PrivateRoute path='/sign-up' component={SignUp} redirectAutenticatedTo='/teams' />
              <PrivateRoute path='/forgot-password' component={ForgotPassword} redirectAutenticatedTo='/teams' />
              <PrivateRoute path='/teams/:teamId/planning-poker' needAuth={true} component={PlanningPoker} />
              <PrivateRoute path='/teams/:teamId' needAuth={true} component={TeamDetails} />
              <PrivateRoute path='/teams' needAuth={true} component={Teams} />
              <PrivateRoute path='*' component={SignIn} redirectAutenticatedTo='/teams' />
            </Switch>
          </Router>
        </UserContext.Provider>
        <DialogConsumer>
          {
            ({ isShow, props, hideDialog }) => {
              return (
                <MyDialog
                  open={isShow}
                  title={props.title}
                  message={props.message}
                  onOk={props.onOk}
                  onCancel={props.onCancel}
                />
              )
            }
          }
        </DialogConsumer>
      </DialogProvider>
    </ThemeProvider>
  )
}

export default App;
