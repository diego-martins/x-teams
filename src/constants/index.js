export const EMOTION_ANGRY = 1
export const EMOTION_BAD = 2
export const EMOTION_OK = 3
export const EMOTION_GOOD = 4
export const EMOTION_HAPPY = 5

export const ATTENTION_RED = 1
export const ATTENTION_YELLOW = 2
export const ATTENTION_GREEN = 3
