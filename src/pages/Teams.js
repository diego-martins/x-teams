import React, { useState, useEffect } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Badge,
  CircularProgress
} from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import TeamService from '../services/teamService'
import AuthService from '../services/authService'
import EmptyList from '../components/EmptyList'
import CreateTeamDialog from '../components/CreateTeamDialog'
import * as emotionUtils from '../utils/emotionUtils'
import { blue } from '@material-ui/core/colors'
import MyProfileDialog from '../components/MyProfileDialog'
import { getInitialUserName } from '../utils/userUtils'
import { useTranslation } from 'react-i18next'
import { stringToColor } from '../utils/colorUtils'
import MyButton from '../components/MyButton'
import { Online, Offline } from 'react-detect-offline'

const SmallAvatar = withStyles(theme => ({
  root: {
    width: 22,
    height: 22,
    border: `1px solid ${theme.palette.background.paper}`,
  },
}))(Avatar)

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  rootPaper: {
    padding: theme.spacing(3, 2),
  },
  rootList: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    paddingTop: '0px',
    paddingBottom: '0px'
  },
  title: {
    flexGrow: 1,
  },
  appBarModal: {
    position: 'relative',
  },
  titleModal: {
    marginLeft: theme.spacing(0),
    flex: 1,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  toobarRightIcon: {
    marginRight: theme.spacing(0),
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600]
  }
}))

export default function Teams() {
  const classes = useStyles()
  const { t } = useTranslation()
  const history = useHistory()
  const [teamList, setTeamList] = useState()
  const [openCreateTeamDialog, setOpenCreateTeamDialog] = useState(false)
  const user = AuthService.getCurrentUser()
  const [openMyProfile, setOpenMyProfile] = useState(false)
  const [loadingTeam, setLoadingTeam] = useState(false)

  const onClickTeam = (history, teamId) => {
    history.push(`/teams/${teamId}`)
  }

  const _loadTeamList = React.useCallback(() => {
    setLoadingTeam(true)
    TeamService.getAll(user.id)
      .then(items => {
        setLoadingTeam(false)
        setTeamList(items)
      })
      .catch(err => {
        setLoadingTeam(false)
        setTeamList([])
        console.error(err)
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user.id])

  useEffect(() => {
    _loadTeamList()
  }, [_loadTeamList])

  const onCloseCreateTeamDialog = (data) => {
    setOpenCreateTeamDialog(false)
    if (data != null) {
      _loadTeamList()
    }
  }

  const renderList = (items) => {

    if (loadingTeam) {
      return (
        <div style={{ textAlign: 'center', padding: '20px' }}>
          <CircularProgress size={40} />
        </div>
      )
    }

    if (!items) {
      return (<div></div>)
    }

    if (!items.length) {
      return (
        <React.Fragment>
          <Online>
            <EmptyList title={t('no_team')} btnText={t('btn_create_team')} onClick={() => setOpenCreateTeamDialog(true)} />
          </Online>
          <Offline>
            <EmptyList title={t('error_offline')} />
          </Offline>
        </React.Fragment>
      )
    }

    return (
      <React.Fragment>
        <List className={classes.rootList}>
          {
            items.map((item, key) => {
              const color = stringToColor(item.name)
              return (
                <ListItem button key={key} onClick={() => onClickTeam(history, item.id)}>
                  <ListItemAvatar>
                    <Badge
                      invisible={false}
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                      }}
                      badgeContent={<SmallAvatar
                        src={emotionUtils.getEmotionIcon(item.emotion, item.attention)}
                        style={emotionUtils.getAttentionStyleColor(item.attention)}
                      />}
                    >
                      <Avatar style={{ backgroundColor: color.back, color: color.front }}>
                        {getInitialUserName(item.name)}
                      </Avatar>
                    </Badge>
                  </ListItemAvatar>
                  <ListItemText primary={item.name} secondary={emotionUtils.getSummary(t, item.emotion, item.attention)} />
                </ListItem>
              )
            })
          }
          <Online>
            <ListItem key={-1} >
              <MyButton
                onClick={() => setOpenCreateTeamDialog(true)}
                color="primary"
                variant="contained"
                rounded={true}
                sizeMicro={true}>
                {t('btn_create_new_team')}
              </MyButton>
            </ListItem>
          </Online>
        </List>

      </React.Fragment>
    )
  }

  const userColor = stringToColor(user.name)
  return (
    <div className={classes.root}>
      <AppBar position='sticky'>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {t('title_teams')}
          </Typography>
          <IconButton edge="end" color="inherit" aria-label="my profile" onClick={() => setOpenMyProfile(true)}>
            <Avatar className={classes.avatar} style={{ backgroundColor: userColor.back, color: userColor.front }}>
              {getInitialUserName(user.name)}
            </Avatar>
          </IconButton>
        </Toolbar>
      </AppBar>

      {renderList(teamList)}

      <CreateTeamDialog
        open={openCreateTeamDialog}
        onClose={onCloseCreateTeamDialog}
        data={{}}
      />

      <MyProfileDialog
        user={user}
        open={openMyProfile}
        onClose={() => setOpenMyProfile(false)}
      />

    </div>
  )
}
