import React, { useState } from 'react'
import {
    Avatar,
    TextField,
    Link,
    Grid,
    Typography,
    Container
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'
import AuthService from '../services/authService'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import MyButton from '../components/MyButton'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(2, 0, 2),
    },
}))

export default function ForgotPassword() {
    const classes = useStyles()
    const { t } = useTranslation()
    const history = useHistory()
    const [loading, setLoading] = useState(false)
    const { register, handleSubmit, errors } = useForm()
    const { showDialog } = React.useContext(DialogContext)

    const onSubmit = (data) => {
        setLoading(true)
        AuthService.resetPassword(data.email)
            .then(() => {
                setLoading(false)
                history.push('/sign-in')
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t('title_forgot_password')}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>
                    <TextField
                        variant="outlined"
                        fullWidth
                        id="email"
                        label={t('hint_email')}
                        name="email"
                        autoComplete="email"
                        autoFocus
                        error={!!errors.email}
                        helperText={errors.email ? errors.email.message : ''}
                        inputProps={{ maxLength: "100" }}
                        inputRef={register({
                            required: t('error_required_field'),
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                message: t('error_invalid_email')
                            }
                        })}
                    />
                    <MyButton
                        loading={loading}
                        type="submit"
                        fullWidth
                        size="large"
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        {t('btn_reset_password')}
                    </MyButton>
                    <Grid container justify="center">
                        <Grid item>
                            <Link href="/sign-in" variant="body2">
                                {t('sign_in')}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}