import React, { useState } from 'react'
import {
    Avatar,
    TextField,
    Link,
    Grid,
    Typography,
    Container
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core/styles'
import AuthService from '../services/authService'
import UserService from '../services/userService'
import { useHistory } from 'react-router-dom'
import { EMOTION_HAPPY, ATTENTION_GREEN } from '../constants'
import MyButton from '../components/MyButton'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(2, 0, 2),
    },
}))

export default function SignUp() {
    const classes = useStyles()
    const history = useHistory()
    const { t } = useTranslation()
    const { register, handleSubmit, errors } = useForm()
    const [loading, setLoading] = useState(false)
    const { showDialog } = React.useContext(DialogContext)

    const onSubmit = (data) => {
        setLoading(true)
        AuthService.signUp(data.email.toLowerCase(), data.password)
            .then(user => {
                if (!user) {
                    return
                }

                user.updateProfile({
                    displayName: `${data.firstName} ${data.lastName}`
                })

                return UserService.create(
                    user.uid,
                    data.firstName,
                    data.lastName,
                    data.email.toLowerCase(),
                    EMOTION_HAPPY,
                    ATTENTION_GREEN)
                    .then(() => {
                        setLoading(false)
                        history.push('/sign-in')
                    })
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t('title_sign_up')}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="firstName"
                                variant="outlined"
                                fullWidth
                                id="firstName"
                                label={t('hint_first_name')}
                                autoFocus
                                inputProps={{ maxLength: "20" }}
                                error={!!errors.firstName}
                                helperText={errors.firstName ? errors.firstName.message : ''}
                                inputRef={register({
                                    required: t('error_required_field')
                                })}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="lastName"
                                label={t('hint_last_name')}
                                name="lastName"
                                autoComplete="lname"
                                inputProps={{ maxLength: "20" }}
                                error={!!errors.lastName}
                                helperText={errors.lastName ? errors.lastName.message : ''}
                                inputRef={register({
                                    required: t('error_required_field')
                                })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="email"
                                label={t('hint_email')}
                                name="email"
                                autoComplete="email"
                                inputProps={{ maxLength: "100" }}
                                error={!!errors.email}
                                helperText={errors.email ? errors.email.message : ''}
                                onChange={e => {
                                    if (e.target.value) {
                                        e.target.value = e.target.value.toLowerCase();
                                    }
                                }}
                                inputRef={register({
                                    required: t('error_required_field'),
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                        message: t('error_invalid_email')
                                    }
                                })}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                fullWidth
                                name="password"
                                label={t('hint_password')}
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                inputProps={{ maxLength: "50" }}
                                error={!!errors.password}
                                helperText={errors.password ? errors.password.message : ''}
                                inputRef={register({
                                    required: t('error_required_field'),
                                    minLength: {
                                        value: 6,
                                        message: t('error_password_length')
                                    }
                                })}
                            />
                        </Grid>
                    </Grid>
                    <MyButton
                        loading={loading}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        size="large"
                        className={classes.submit}
                    >
                        {t('btn_sign_up')}
                    </MyButton>
                    <Grid container justify="center">
                        <Grid item>
                            <Link href="/sign-in" variant="body2">
                                {t('sign_in')}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}