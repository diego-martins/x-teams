import React, { useState, useEffect, useCallback } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Paper,
  Grid,
  Badge,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Menu,
  MenuItem,
  ListItemSecondaryAction,
  CircularProgress
} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import BackIcon from '@material-ui/icons/ArrowBack'
import { useHistory, useParams } from 'react-router-dom'
import TeamService from '../services/teamService'
import MemberService from '../services/memberService'
import * as emotionUtils from '../utils/emotionUtils'
import AuthService from '../services/authService'
import SelfEvaluationDialog from '../components/SelfEvaluationDialog'
import AddMemberDialog from '../components/AddMemberDialog'
import { getInitialUserName } from '../utils/userUtils'
import { useTranslation } from 'react-i18next'
import { stringToColor } from '../utils/colorUtils'
import MyButton from '../components/MyButton'
import PlanningSettingsDialog from '../components/PlanningSettingsDialog'
import CreateTeamDialog from '../components/CreateTeamDialog'
import { DialogContext } from '../contexts/DialogContext'

const SmallAvatar = withStyles(theme => ({
  root: {
    width: 22,
    height: 22,
    border: `1px solid ${theme.palette.background.paper}`,
  },
}))(Avatar)

const SmallAvatarTeam = withStyles(theme => ({
  root: {
    width: 30,
    height: 30,
    border: `1px solid ${theme.palette.background.paper}`,
  },
}))(Avatar)

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
    margin: '10px',
    '& .MuiCardHeader-action': {
      marginTop: '0px'
    }
  },
  cardTeam: {
    margin: '10px',
    '& .MuiCardContent-root': {
      padding: '0px'
    },
    '& .avatar-team': {
      width: '60px',
      height: '60px',
      fontSize: '1.8rem'
    },
    '& .MuiListItemText-primary': {
      marginLeft: '10px',
      fontSize: '1.5rem'
    },
    '& .MuiListItemText-secondary': {
      marginLeft: '12px'
    }
  },
  rootPaper: {
    padding: theme.spacing(3, 2),
  },
  rootContainer: {
  },
  rootList: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    paddingTop: '0px',
    paddingBottom: '0px'
  },
  title: {
    flexGrow: 1,
  },
  appBarModal: {
    position: 'relative',
  },
  titleModal: {
    marginLeft: theme.spacing(0),
    flex: 1,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  toobarRightIcon: {
    marginRight: theme.spacing(1),
  },
  paperTeam: {
    display: 'flex',
    textAlign: 'center',
    flexDirection: 'column',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText
  },
  planningPokerContainer: {
    padding: '10px',
    display: 'flex',
    textAlign: 'center',
    flexDirection: 'column'
  },
  bigAvatar: {
    width: 60,
    height: 60,
    margin: 'auto'
  },
  startButton: {
    width: '100%'
  }
}))

const onClickBack = (history) => {
  history.push('/teams')
}

export default function TeamDetails() {
  const history = useHistory()
  const classes = useStyles()
  const { t } = useTranslation()
  const { teamId } = useParams()
  const [team, setTeam] = useState()
  const [memberList, setMemberList] = useState()
  const [openAddMemberDialog, setOpenAddMemberDialog] = useState(false)
  const [currentMember, setCurrentMember] = useState(null)
  const [selectedEmotionDialogValue, setSelectedEmotionDialogValue] = React.useState(0)
  const [selectedAttentionDialogValue, setSelectedAttentionDialogValue] = React.useState(0)
  const [openSelfEvaluationDialog, setOpenSelfEvaluationDialog] = React.useState(false)
  const [anchorMemberMenu, setAnchorMemberMenu] = useState({ target: null, item: null })
  const [anchorSettingsMenu, setAnchorSettingsMenu] = useState({ target: null })
  const [openSettingsDialog, setOpenSettingsDialog] = React.useState(false)
  const [openEditTeamDialog, setOpenEditTeamDialog] = useState(false)
  const [loadingTeam, setLoadingTeam] = useState(false)
  const [loadingMembers, setLoadingMembers] = useState(false)
  const user = AuthService.getCurrentUser()
  const { showDialog, hideDialog } = React.useContext(DialogContext)

  const onClickMemberItem = (member) => {
    if (member.id === user.id) {
      setOpenSelfEvaluationDialog(true)
    }
  }

  const onClickPlanningPoker = () => {
    history.push(`/teams/${teamId}/planning-poker`)
  }

  const onCloseSelfEvaluationDialog = (data) => {
    setOpenSelfEvaluationDialog(false)
    if (data) {
      _loadMemberList()
    }
  }

  const onOpenMemberMenu = (event, item) => {
    setAnchorMemberMenu({
      target: event.currentTarget,
      item
    })
  }

  const onCloseMemberMenu = () => {
    setAnchorMemberMenu({ target: null, item: null })
  }

  const onOpenSettingsMenu = (event) => {
    setAnchorSettingsMenu({
      target: event.currentTarget
    })
  }

  const onCloseSettingsMenu = () => {
    setAnchorSettingsMenu({ target: null })
    setOpenSettingsDialog(false)
  }

  const onEditTeam = () => {
    setAnchorSettingsMenu({ target: null })

    if (!currentMember || !currentMember.admin) {
      showDialog({
        message: t('error_only_admin_edit_team')
      })
      return
    }

    setOpenEditTeamDialog(true)
  }

  const onCloseEditTeamDialog = (changed) => {
    setOpenEditTeamDialog(false)
    if (changed) {
      TeamService.getById(teamId)
        .then(teamResult => {
          setTeam(teamResult)
        })
    }
  }

  const onDeleteTeam = () => {
    setAnchorSettingsMenu({ target: null })
    showDialog({
      message: t('confirm_delete_team', { name: team.name }),
      onOk: () => {
        deleteTeam()
        hideDialog()
      },
      onCancel: () => hideDialog()
    })
  }

  const deleteTeam = () => {
    MemberService.getAll(teamId)
      .then(members => {
        if (!currentMember || !currentMember.admin) {
          showDialog({ message: t('error_only_admin_delete_team') })
          return
        }

        if (members && members.length > 1) {
          showDialog({ message: t('error_delete_not_empty_team') })
          return
        }

        return TeamService.delete(teamId)
          .then(() => {
            history.push('/teams')
          })
      })
      .catch(err => showDialog({ message: err.message }))
  }

  const onDeleteMember = (member) => {
    onCloseMemberMenu()
    if (!member || !currentMember || !currentMember.admin) {
      return
    }

    if (member.id === currentMember.id) {
      showDialog({ message: t('error_delete_your_self') })
      return
    }

    showDialog({
      message: t('confirm_delete_member', { name: member.name }),
      onOk: () => {
        MemberService.delete(teamId, member.id)
          .then(() => {
            _loadMemberList()
          })
          .catch(err => showDialog({ message: err.message }))
        hideDialog()
      },
      onCancel: () => hideDialog()
    })
  }

  const _loadMemberList = useCallback(() => {
    setLoadingTeam(true)
    setLoadingMembers(true)
    TeamService.getById(teamId)
      .then(teamResult => {
        setLoadingTeam(false)
        setTeam(teamResult)

        MemberService.getAll(teamId)
          .then(items => {
            setLoadingMembers(false)
            const userInMemberList = items.find(item => item.id === user.id)

            if (userInMemberList) {
              setSelectedEmotionDialogValue(prev =>
                prev !== userInMemberList.emotion ? userInMemberList.emotion : prev
              )
              setSelectedAttentionDialogValue(prev =>
                prev !== userInMemberList.attention ? userInMemberList.attention : prev
              )
            }

            let self = items.find(item => item.id === userInMemberList.id)
            let _items = items.filter(item => item.id !== userInMemberList.id)

            setMemberList([self].concat(_items))
            setCurrentMember(userInMemberList)
          })
          .catch(err => {
            setLoadingMembers(false)
            showDialog({ message: err.message })
          })
      })
      .catch(err => {
        setLoadingTeam(false)
        setLoadingMembers(false)
        showDialog({ message: err.message })
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [teamId, user.id])

  useEffect(() => {
    _loadMemberList()
  }, [_loadMemberList])

  const renderMemberList = (items) => {

    if (loadingMembers) {
      return (
        <div style={{ textAlign: 'center', padding: '20px' }}>
          <CircularProgress size={40} />
        </div>
      )
    }

    if (!items) {
      return (<div></div>)
    }

    if (!items.length) {
      return (
        <Typography variant="h6">{t('no_members')}</Typography>
      )
    }

    return (
      <React.Fragment>
        <List className={classes.rootList}>
          {
            items.map((item, key) => {
              const color = stringToColor(item.name)
              return (
                <ListItem
                  button={item.id === user.id}
                  key={key}
                  onClick={() => onClickMemberItem(item)}
                >
                  <ListItemAvatar>
                    <Badge
                      invisible={false}
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right'
                      }}
                      badgeContent={<SmallAvatar
                        src={emotionUtils.getEmotionIcon(item.emotion, item.attention)}
                        style={emotionUtils.getAttentionStyleColor(item.emotion)}
                      />}
                    >
                      <Avatar style={{ backgroundColor: color.back, color: color.front }}>
                        {getInitialUserName(item.name)}
                      </Avatar>
                    </Badge>
                  </ListItemAvatar>
                  <ListItemText primary={item.id === user.id ? t('you') : item.name} secondary={emotionUtils.getMemberSummary(t, item)} />
                  <ListItemSecondaryAction>
                    {
                      currentMember && currentMember.admin && currentMember.id !== item.id &&
                      <IconButton edge="end" aria-label="member options" onClick={(e) => onOpenMemberMenu(e, item)}>
                        <MoreVertIcon />
                      </IconButton>
                    }
                  </ListItemSecondaryAction>
                </ListItem>
              )
            })
          }
        </List>
        <Menu
          id="member-menu"
          anchorEl={anchorMemberMenu.target}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          keepMounted
          open={Boolean(anchorMemberMenu.target)}
          onClose={onCloseMemberMenu}
        >
          <MenuItem onClick={() => onDeleteMember(anchorMemberMenu.item)}>{t('btn_delete')}</MenuItem>
        </Menu>
      </React.Fragment>
    )
  }

  const renderCardMembers = (items) => {
    return (
      <Grid
        container
        spacing={0}
        alignItems="center"
        justify="center"
      >
        <Grid item xs={12} sm={9} md={6}>
          <Card className={classes.card}>
            <CardHeader
              title={t('members')}
              action={
                <IconButton aria-label="add member"
                  disabled={!currentMember || !currentMember.admin || loadingMembers}
                  onClick={onClickOpenCreateMember}
                >
                  <AddIcon />
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              {renderMemberList(memberList)}
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    )
  }

  const onCloseAddMemberDialog = (data) => {
    setOpenAddMemberDialog(false)
    if (data) {
      _loadMemberList()
    }
  }

  const onClickOpenCreateMember = () => {
    if (currentMember && currentMember.admin) {
      setOpenAddMemberDialog(true)
    }
  }

  const renderTeam = () => {

    if (loadingTeam) {
      return (
        <Paper className={classes.paperTeam} square={true} elevation={0}>
          <Grid container justify="center">
            <Grid item xs={12} sm={9} md={6}>
              <Card className={classes.cardTeam} style={{ paddingTop: '16px', paddingBottom: '16px' }}>
                <CardContent>
                  <CircularProgress size={60} />
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Paper>
      )
    }

    if (!team) return null

    const color = stringToColor(team.name)
    return (
      <Paper className={classes.paperTeam} square={true} elevation={0}>
        <Grid container justify="center">
          <Grid item xs={12} sm={9} md={6}>
            <Card className={classes.cardTeam}>
              <CardContent>
                <ListItem>
                  <ListItemAvatar>
                    <Badge
                      invisible={false}
                      overlap="circle"
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right'
                      }}
                      badgeContent={<SmallAvatarTeam
                        src={emotionUtils.getEmotionIcon(team.emotion, team.attention)}
                        style={emotionUtils.getAttentionStyleColor(team.emotion)}
                      />}
                    >
                      <Avatar className={'avatar-team'} style={{ backgroundColor: color.back, color: color.front }}>
                        {getInitialUserName(team.name)}
                      </Avatar>
                    </Badge>
                  </ListItemAvatar>
                  <ListItemText primary={team.name} secondary={
                    <div>
                      <div>{team.company}</div>
                      <div>{emotionUtils.getMemberSummary(t, team)}</div>
                    </div>
                  } />
                </ListItem>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Paper>
    )
  }

  const renderPlanningButton = () => {
    return (
      <Paper className={classes.planningPokerContainer} square={true} elevation={0}>
        <Grid
          container
          spacing={0}
          alignItems="center"
          justify="center"
        >
          <Grid item xs={12} sm={9} md={6}>
            <MyButton
              size="large"
              color="primary"
              aria-label="start planning poker"
              variant="contained"
              className={classes.startButton}
              onClick={onClickPlanningPoker}
              disabled={loadingTeam || loadingMembers}
            >
              {t('btn_start_planning_poker')}
            </MyButton>
          </Grid>
        </Grid>
      </Paper>
    )
  }

  return (
    <div className={classes.root}>
      <AppBar position='sticky' elevation={0}>
        <Toolbar>
          <IconButton onClick={() => onClickBack(history)} edge="start" color="inherit" aria-label="back to teams">
            <BackIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {t('title_team')}
          </Typography>
          {
            currentMember && currentMember.admin &&
            <IconButton edge="end" color="inherit" aria-label="team options" onClick={onOpenSettingsMenu}>
              <MoreVertIcon />
            </IconButton>
          }
        </Toolbar>

        <Menu
          id="options-menu"
          anchorEl={anchorSettingsMenu.target}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          keepMounted
          open={Boolean(anchorSettingsMenu.target)}
          onClose={onCloseSettingsMenu}
        >
          <MenuItem onClick={() => { setOpenSettingsDialog(true) }}>{t('btn_planning')}</MenuItem>
          <MenuItem onClick={onEditTeam}>{t('btn_edit')}</MenuItem>
          <MenuItem onClick={onDeleteTeam}>{t('btn_delete')}</MenuItem>
        </Menu>

      </AppBar>

      {renderTeam()}

      {renderPlanningButton()}

      {renderCardMembers(memberList)}

      <AddMemberDialog
        open={openAddMemberDialog}
        onClose={onCloseAddMemberDialog}
        teamId={teamId}
      />

      <SelfEvaluationDialog
        open={openSelfEvaluationDialog}
        onClose={onCloseSelfEvaluationDialog}
        teamId={teamId}
        emotionValue={selectedEmotionDialogValue}
        attentionValue={selectedAttentionDialogValue}
      />

      <PlanningSettingsDialog
        teamId={teamId}
        open={openSettingsDialog}
        onClose={onCloseSettingsMenu}
      />

      <CreateTeamDialog
        open={openEditTeamDialog}
        onClose={onCloseEditTeamDialog}
        data={team}
      />

    </div >
  )
}
