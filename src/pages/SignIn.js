import React, { useState } from 'react'
import {
    Avatar,
    TextField,
    Link,
    Grid,
    Typography,
    Container
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'
import AuthService from '../services/authService'
import MyButton from '../components/MyButton'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(2, 0, 2),
    },
}))

const getDeepError = (err) => {
    try {
        const parsed = JSON.parse(err.message)
        return parsed.error.message
    } catch (e) {
        return err.message || err
    }
}

function SignIn() {
    const classes = useStyles()
    const { t } = useTranslation()
    const history = useHistory()
    const { register, handleSubmit, errors } = useForm()
    const [loading, setLoading] = useState(false)
    const { showDialog } = React.useContext(DialogContext)

    const onSubmit = (data) => {
        setLoading(true)
        AuthService.signIn(data.email.toLowerCase(), data.password)
            .then(result => {
                setLoading(false)
                history.push('/teams')
            })
            .catch(err => {
                setLoading(false)
                console.error('err', err)
                showDialog({ message: getDeepError(err) })
            })
    }

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t('title_sign_in')}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="email"
                        label={t('hint_email')}
                        name="email"
                        autoComplete="email"
                        autoFocus
                        inputProps={{ maxLength: "100" }}
                        error={!!errors.email}
                        helperText={errors.email ? errors.email.message : ''}
                        onChange={e => {
                            if (e.target.value) {
                                e.target.value = e.target.value.toLowerCase();
                            }
                        }}
                        inputRef={register({
                            required: t('error_required_field'),
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                message: t('error_invalid_email')
                            }
                        })}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="password"
                        label={t('hint_password')}
                        type="password"
                        id="password"
                        inputProps={{ maxLength: "50" }}
                        error={!!errors.password}
                        helperText={errors.password ? errors.password.message : ''}
                        inputRef={register({
                            required: t('error_required_field')
                        })}
                    />
                    <MyButton
                        loading={loading}
                        type="submit"
                        fullWidth
                        variant="contained"
                        size="large"
                        color="primary"
                        className={classes.submit}
                    >
                        {t('btn_sign_in')}
                    </MyButton>
                    <Grid container>
                        <Grid item xs>
                            <Link href="/forgot-password" variant="body2">
                                {t('forgot_password')}
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link href="/sign-up" variant="body2">
                                {t('sign_up')}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    )
}

export default SignIn