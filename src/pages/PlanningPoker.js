import React from 'react'
import {
    Container,
    AppBar,
    Toolbar,
    Typography,
    Grid,
    IconButton,
    CircularProgress
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import CardPoker from '../components/CardPoker'
import LocalCafeIcon from '@material-ui/icons/LocalCafe'
import BackIcon from '@material-ui/icons/ArrowBack'
import { useHistory, useParams } from 'react-router-dom'
import PlanningPokerService from '../services/planningPokerService'
import AuthService from '../services/authService'
import { DialogContext } from '../contexts/DialogContext'
import _ from 'lodash'
import { tsToMillis } from '../utils/firebaseUtils'
import MyButton from '../components/MyButton'
import moment from 'moment'
import SimplePlayersDialog from '../components/SimplePlayersDialog'
import { useTranslation } from 'react-i18next'
import Countdown from 'react-countdown'
import Matrix from '../components/Matrix'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        marginBottom: '60px'
    },
    title: {
        flexGrow: 1,
    },
    cardFooter: {
        top: 'auto',
        bottom: '0px',
        position: 'absolute',
        width: '95%',
        paddingRight: '16px'
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    average: {
        textAlign: 'right',
        marginRight: '8px',
        textTransform: 'lowercase'
    }
}))

export default function PlanningPoker() {
    const classes = useStyles()
    const { t } = useTranslation()
    const [selectedCard, setSelectedCard] = React.useState()
    const history = useHistory()
    const { teamId } = useParams()
    const { showDialog } = React.useContext(DialogContext)
    const [players, setPlayers] = React.useState([])
    const [teamCards, setTeamCards] = React.useState([])
    const [loadingMyMove, setLoadingMyMove] = React.useState({})
    const [loadingTeamMove, setLoadingTeamMove] = React.useState(false)
    const maxTime = 10
    const [time, setTime] = React.useState(maxTime)
    const [running, setRunning] = React.useState(false)
    const [loadingStart, setLoadingStart] = React.useState(false)
    const [loadingCancel, setLoadingCancel] = React.useState(false)
    const [finishedAt, setFinishedAt] = React.useState(null)
    const [showPlayersDialog, setShowPlayersDialog] = React.useState({})
    const [whoStartedId, setWhoStartedId] = React.useState()
    const user = AuthService.getCurrentUser()
    const minToCancell = 3 //allow cancel if more than 3 seg
    const [cardSequence, setCardSequence] = React.useState()
    const [loadingSequence, setLoadingSequence] = React.useState(true)
    const [averageCard, setAverageCard] = React.useState()
    const [playWith, setPlayWith] = React.useState('cards')

    const getTShirtSequence = () => [
        { order: 1, value: 1, players: [], text: 'XS', backColor: '#E57373', frontColor: '#fff' },
        { order: 2, value: 2, players: [], text: 'S', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 3, value: 3, players: [], text: 'M', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 4, value: 4, players: [], text: 'L', backColor: '#78909C', frontColor: '#fff' },
        { order: 5, value: 5, players: [], text: 'XL', backColor: '#F44336', frontColor: '#fff' },
        { order: 6, value: 6, players: [], text: 'XXL', backColor: '#4CAF50', frontColor: '#fff' }
    ]

    const getNaturalSequence = (t) => [
        { order: 1, value: 0, players: [], text: '0', backColor: '#E57373', frontColor: '#fff' },
        { order: 2, value: 1, players: [], text: '1', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 3, value: 2, players: [], text: '2', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 4, value: 3, players: [], text: '3', backColor: '#78909C', frontColor: '#fff' },
        { order: 5, value: 4, players: [], text: '4', backColor: '#F44336', frontColor: '#fff' },
        { order: 6, value: 5, players: [], text: '5', backColor: '#4CAF50', frontColor: '#fff' },
        { order: 7, value: 6, players: [], text: '6', backColor: '#E57373', frontColor: '#fff' },
        { order: 8, value: 7, players: [], text: '7', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 9, value: 8, players: [], text: '8', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 10, value: 9, players: [], text: '9', backColor: '#78909C', frontColor: '#fff' },
        { order: 11, value: 10, players: [], text: '10', backColor: '#F44336', frontColor: '#fff' },
        { order: 12, value: -1, players: [], text: '?', backColor: '#4CAF50', frontColor: '#fff' },
        { order: 13, value: -2, players: [], icon: LocalCafeIcon, text: t('card_coffe'), backColor: '#333', frontColor: '#fff' }
    ]

    const getStandardSequence = (t) => [
        { order: 1, value: 0, players: [], text: '0', backColor: '#E57373', frontColor: '#fff' },
        { order: 2, value: 0.5, players: [], text: '1/2', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 3, value: 1, players: [], text: '1', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 4, value: 2, players: [], text: '2', backColor: '#78909C', frontColor: '#fff' },
        { order: 5, value: 3, players: [], text: '3', backColor: '#F44336', frontColor: '#fff' },
        { order: 6, value: 5, players: [], text: '5', backColor: '#4CAF50', frontColor: '#fff' },
        { order: 7, value: 8, players: [], text: '8', backColor: '#E57373', frontColor: '#fff' },
        { order: 8, value: 13, players: [], text: '13', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 9, value: 20, players: [], text: '20', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 10, value: 40, players: [], text: '40', backColor: '#78909C', frontColor: '#fff' },
        { order: 11, value: 100, players: [], text: '100', backColor: '#F44336', frontColor: '#fff' },
        { order: 12, value: -1, players: [], text: '?', backColor: '#4CAF50', frontColor: '#fff' },
        { order: 13, value: -2, players: [], icon: LocalCafeIcon, text: t('card_coffe'), backColor: '#333', frontColor: '#fff' }
    ]

    const getFibonacciSequence = (t) => [
        { order: 1, value: 0, players: [], text: '0', backColor: '#E57373', frontColor: '#fff' },
        { order: 2, value: 1, players: [], text: '1', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 3, value: 2, players: [], text: '2', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 4, value: 3, players: [], text: '3', backColor: '#78909C', frontColor: '#fff' },
        { order: 5, value: 5, players: [], text: '5', backColor: '#F44336', frontColor: '#fff' },
        { order: 6, value: 8, players: [], text: '8', backColor: '#4CAF50', frontColor: '#fff' },
        { order: 7, value: 13, players: [], text: '13', backColor: '#E57373', frontColor: '#fff' },
        { order: 8, value: 21, players: [], text: '21', backColor: '#8BC34A', frontColor: '#fff' },
        { order: 9, value: 34, players: [], text: '34', backColor: '#9C27B0', frontColor: '#fff' },
        { order: 10, value: 55, players: [], text: '55', backColor: '#78909C', frontColor: '#fff' },
        { order: 11, value: -1, players: [], text: '?', backColor: '#F44336', frontColor: '#fff' },
        { order: 12, value: -2, players: [], icon: LocalCafeIcon, text: t('card_coffe'), backColor: '#333', frontColor: '#fff' }
    ]

    const getAllTeamCards = React.useCallback(() => {
        switch (cardSequence) {
            case 'fibonacci':
                return getFibonacciSequence(t)
            case 'natural':
                return getNaturalSequence(t)
            case 't-shirt':
                return getTShirtSequence()
            case 'standard':
            default:
                return getStandardSequence(t)
        }
    }, [cardSequence, t])

    const myCards = getAllTeamCards()

    const onStartButton = () => {
        setLoadingStart(true)
        PlanningPokerService.start(
            teamId,
            user.id,
            user.name,
            players.map(item => item.id),
            moment().utc().valueOf(),
            moment().utc().add(maxTime, 's').add(500, 'ms').valueOf())
            .then(() => {
                setLoadingStart(false)
                setRunning(true)
            })
            .catch(err => {
                console.error(err)
                setLoadingStart(false)
            })
    }

    const onCancelButton = () => {
        setLoadingCancel(true)
        PlanningPokerService.cancel(
            teamId,
            user.id,
            players.map(item => item.id),
            moment().utc().valueOf())
            .then(() => {
                setRunning(false)
                setSelectedCard(null)
                setLoadingCancel(false)
            })
            .catch(err => {
                console.error(err)
                setLoadingCancel(false)
            })
    }

    const onClickBack = () => {
        PlanningPokerService.updatePlayerCardValue(teamId, user.id, user.name, null)
        history.push(`/teams/${teamId}`)
    }

    const onClickTeamCard = (cardValue, cardText) => {
        if ((cardValue === 0 || cardValue) && !running && players.length) {
            setShowPlayersDialog({
                title: `${t('title_card')} ${cardText || ''}`,
                players: players.filter(item => item.cardValue === cardValue),
                open: true
            })
        }
    }

    const onClickMyCard = (cardValue) => {
        let cardData
        if (selectedCard && selectedCard.value === cardValue) {
            cardData = null
        } else {
            cardData = myCards.find(item => item.value === cardValue)
        }

        setLoadingMyMove({ cardValue, loading: true })
        const lastCardData = selectedCard
        setSelectedCard(cardData)
        PlanningPokerService.updatePlayerCardValue(
            teamId, user.id, user.name, cardValue)
            .then(() => {
                setLoadingMyMove({ loading: false })
            })
            .catch(err => {
                console.error(err)
                setSelectedCard(lastCardData)
                setLoadingMyMove({ loading: false })
                showDialog({ message: t('error_move_my_card') })
            })
    }

    const onClickMyMatrix = (item) => {
        const cardValue = item.value
        let cardData
        if (selectedCard && selectedCard.value === cardValue) {
            cardData = null
        } else {
            cardData = myCards.find(item => item.value === cardValue)
            cardData.matrixId = item.id
        }

        setLoadingMyMove({ cardValue, loading: true })
        const lastCardData = selectedCard
        setSelectedCard(cardData)
        PlanningPokerService.updatePlayerCardValue(
            teamId, user.id, user.name, cardValue)
            .then(() => {
                setLoadingMyMove({ loading: false })
            })
            .catch(err => {
                console.error(err)
                setSelectedCard(lastCardData)
                setLoadingMyMove({ loading: false })
                showDialog({ message: t('error_move_my_card') })
            })
    }

    const calculateAverage = (cards) => {
        let total = 0
        const _cards = cards.filter(item => item.players.length).map(item => item.value)
        if (!_cards.length) {
            setAverageCard(null)
            return
        }

        _cards.forEach(item => total += item)
        let totalAverage = Math.round(total / _cards.length)
        const _averageCard = cards.find(item => item.value >= totalAverage)
        setAverageCard(_averageCard)
    }

    React.useEffect(() => {
        setLoadingTeamMove(true)
        const allCards = getAllTeamCards()
        setTeamCards(allCards)

        setLoadingSequence(true)
        PlanningPokerService.getById(teamId)
            .then(data => {
                setCardSequence(data.cardSequence || 'standard')
                setLoadingSequence(false)
            })
            .catch(err => {
                setLoadingSequence(false)
                console.error(err)
            })

        const observer = PlanningPokerService.observerAllPlayers(teamId)
            .onSnapshot(snaps => {
                const _players = []
                let _currPlayer
                let _whoStarted
                snaps.forEach(doc => {
                    const _player = { id: doc.id, ...doc.data() }
                    if (_player.id === user.id) {
                        _currPlayer = _player
                    }

                    if (_player.startedAt && _player.finishedAt) {
                        if (!_whoStarted) {
                            _whoStarted = _player
                        } else if (_whoStarted.startedAt && _whoStarted.finishedAt) {
                            if (tsToMillis(_player.startedAt) < tsToMillis(_whoStarted.startedAt)) {
                                _whoStarted = _player
                            }
                        }
                    }
                    _players.push(_player)
                })
                const groupByValue = _.groupBy(_players, 'cardValue')
                const _teamCards = allCards.map(card => ({
                    ...card,
                    players: groupByValue[card.value] || []
                }))

                if (_whoStarted) {
                    setWhoStartedId(_whoStarted.id)
                    if (_whoStarted.canceledAt && tsToMillis(_whoStarted.canceledAt) >= tsToMillis(_whoStarted.startedAt)) {
                        setLoadingStart(false)
                        setRunning(false)
                        setFinishedAt(null)
                        setAverageCard(null)
                    } else {
                        const _finishedAt = tsToMillis(_whoStarted.finishedAt)
                        setFinishedAt(_finishedAt)
                        const _finishAt = moment(_finishedAt).utc()
                        const _currentAt = moment().utc()
                        if (_finishAt.isAfter(_currentAt)) {
                            const restTime = _finishAt.diff(_currentAt, 'second')
                            if (restTime > 0) {
                                setLoadingStart(false)
                                setRunning(true)
                            }
                        }
                    }
                }
                setPlayers(_players)
                setTeamCards(_teamCards)
                calculateAverage(_teamCards)
                setLoadingTeamMove(false)
                if (_currPlayer) {
                    setSelectedCard(prev => {
                        if (!prev || prev.value !== _currPlayer.cardValue) {
                            return allCards.find(item => item.value === _currPlayer.cardValue)
                        }
                        return prev
                    })
                }
            }, err => {
                console.error(err)
                setLoadingTeamMove(false)
            })

        return function () {
            observer()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getAllTeamCards, teamId, user])

    const renderCowntdown = () => {
        return (
            <Countdown
                date={finishedAt}
                intervalDelay={0}
                precision={0}
                autoStart={running}
                renderer={props => `${props.seconds} ${t('abb_seconds')}`}
                onComplete={() => {
                    setRunning(false)
                    calculateAverage(teamCards)
                }}
                onTick={data => {
                    setTime(prev => prev !== data.seconds ? data.seconds : prev)
                }}
            />
        )
    }

    const renderMyCards = (cards) => {

        if (loadingSequence) {
            return (
                <Grid
                    container
                    spacing={0}
                    alignItems="center"
                    justify="center"
                    style={{ marginTop: '10px' }}
                >
                    <Grid item>
                        <CircularProgress size={20} />
                    </Grid>
                </Grid>
            )
        }

        return (
            <Grid
                container
                spacing={0}
                alignItems="center"
                justify="center"
                style={{ marginTop: '10px' }}
            >
                {
                    cards.map(card => (
                        <Grid item key={card.value}>
                            <CardPoker
                                style={{ margin: '2px' }}
                                cardValue={card.value}
                                cardText={card ? card.text : ''}
                                CardIcon={card ? card.icon : null}
                                players={card.players.length}
                                selected={!!selectedCard && card.value === selectedCard.value}
                                noCard={!!selectedCard && card.value === selectedCard.value}
                                onSelect={() => onClickMyCard(card.value)}
                                loading={loadingMyMove.loading && loadingMyMove.cardValue === card.value}
                                disabled={!running}
                                backColor={card.backColor || null}
                                frontColor={card.frontColor || null}
                                size='medium'
                            />
                        </Grid>
                    ))
                }
            </Grid>
        )
    }

    const renderMyMatrix = () => {

        if (loadingSequence) {
            return (
                <Grid
                    container
                    spacing={0}
                    alignItems="center"
                    justify="center"
                    style={{ marginTop: '10px' }}
                >
                    <Grid item>
                        <CircularProgress size={20} />
                    </Grid>
                </Grid>
            )
        }

        return (
            <Matrix
                style={{ marginTop: '20px', marginBottom: '20px' }}
                selectedId={!!selectedCard && selectedCard.matrixId}
                onClick={(item) => onClickMyMatrix(item || {})}
                sequence={cardSequence}
                disabled={!running}
            />
        )
    }

    return (
        <div className={classes.root}>
            <AppBar position='sticky' elevation={0}>
                <Toolbar>
                    <IconButton onClick={() => onClickBack()} edge="start" color="inherit" aria-label="back to teams">
                        <BackIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        {t('title_planning_poker')}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container component="main">
                <Grid
                    container
                    spacing={0}
                    alignItems="center"
                    justify="center"
                    style={{ marginTop: '10px' }}
                >
                    <Grid item xs={12} sm={8} md={6}>
                        <Typography variant="h6" className={classes.title} color='textSecondary'>
                            {t('cards_of_team')}
                        </Typography>
                        <Grid
                            container
                            spacing={0}
                            style={{ marginTop: '10px' }}
                        >
                            <Grid
                                container item
                                spacing={0}
                            >
                                <Grid item xs={10}>
                                    <Grid container>
                                        {
                                            loadingTeamMove &&
                                            <Grid item key={-100}>
                                                <CardPoker
                                                    style={{ margin: '2px' }}
                                                    players={0}
                                                    loading={true}
                                                />
                                            </Grid>
                                        }
                                        {
                                            !loadingTeamMove && !teamCards.filter(item => item.players.length).length &&
                                            <Grid item key={-100}>
                                                <CardPoker
                                                    style={{ margin: '2px' }}
                                                    players={0}
                                                    loading={false}
                                                />
                                            </Grid>
                                        }
                                        {
                                            teamCards.filter(item => item.players.length).map(card => (
                                                <Grid item key={card.value}>
                                                    <CardPoker
                                                        style={{ margin: '2px' }}
                                                        cardText={card ? card.text : ''}
                                                        CardIcon={card ? card.icon : null}
                                                        players={card.players.length}
                                                        backside={running}
                                                        onSelect={() => onClickTeamCard(card.value, card.text)}
                                                        backColor={card.backColor}
                                                        frontColor={card.frontColor}
                                                    />
                                                </Grid>
                                            ))
                                        }
                                    </Grid>
                                </Grid>

                                <Grid item xs={2}>
                                    <Grid container
                                        alignItems="flex-end"
                                        justify="flex-end">
                                        <Grid item style={{ position: 'relative' }}>
                                            <CardPoker
                                                style={{ margin: '2px' }}
                                                players={0}
                                                loading={false}
                                                cardText={!running && averageCard ? averageCard.text : ''}
                                                CardIcon={!running && averageCard ? averageCard.icon : null}
                                                backColor={!running && averageCard ? averageCard.backColor : null}
                                                frontColor={!running && averageCard ? averageCard.frontColor : null}
                                            />
                                            <Typography variant="body2" className={classes.average} color='textSecondary'>
                                                {t('average')}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={0}
                    alignItems="center"
                    justify="center"
                >
                    <Grid item xs={12} sm={8} md={6}>
                        <Grid
                            container
                            spacing={0}
                            alignItems="center"
                            justify="center"
                            style={{ marginTop: '10px' }}
                        >
                            <Grid item xs={6}>
                                <MyButton
                                    type="submit"
                                    variant="contained"
                                    fullWidth
                                    size="large"
                                    color="primary"
                                    className={classes.submit}
                                    onClick={onStartButton}
                                    loading={loadingStart}
                                    disabled={loadingStart || running}
                                    style={{ textTransform: running ? 'lowercase' : 'uppercase' }}
                                >
                                    {
                                        !running ? t('btn_start') : renderCowntdown()
                                    }
                                </MyButton>

                                {
                                    whoStartedId === user.id && running && time >= minToCancell &&
                                    <MyButton
                                        type="submit"
                                        size="small"
                                        color="primary"
                                        fullWidth
                                        className={classes.submit}
                                        onClick={onCancelButton}
                                        loading={loadingCancel}
                                        disabled={loadingCancel || !running}
                                        style={{ marginTop: '10px' }}
                                    >
                                        {t('btn_cancel')}
                                    </MyButton>
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={0}
                    alignItems="center"
                    justify="center"
                    style={{ marginTop: '10px' }}
                >
                    <Grid item xs={12} sm={8} md={6}>
                        <Typography variant="h6" component="span" className={classes.title} color='textSecondary'>
                            {t(playWith === 'matrix' ? 'my_matrix' : 'my_cards')}
                        </Typography>
                        {
                            !loadingSequence && (cardSequence === 'standard' || cardSequence === 'fibonacci') &&
                            <Typography variant="body1" style={{ marginLeft: '5px', color: '#06f', cursor: 'pointer' }} component="span" className={classes.title} color='textSecondary' onClick={() => setPlayWith(prev => prev === 'cards' ? 'matrix' : 'cards')}>
                                ({t(playWith === 'matrix' ? 'use_cards' : 'use_matrix').toLowerCase()})
                            </Typography>
                        }
                        {playWith === 'matrix' ? renderMyMatrix() : renderMyCards(myCards)}
                    </Grid>
                </Grid>
            </Container>
            <SimplePlayersDialog
                title={showPlayersDialog.title || ''}
                players={showPlayersDialog.players || []}
                open={!!showPlayersDialog.open}
                onClose={() => setShowPlayersDialog({})}
            />
        </div>
    )
}