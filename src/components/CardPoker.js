import React from 'react'
import PropTypes from 'prop-types'
import {
    Card,
    CardContent,
    Typography,
    Badge,
    CircularProgress
} from '@material-ui/core'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import bgCard from '../assets/images/bg_card.jpg'
import { hexToHSL } from '../utils/colorUtils'

const StyledBadge = withStyles(theme => ({
    badge: {
        right: 8,
        top: 8,
        border: `1px solid #fff`,
        padding: '0 4px',
        cursor: 'default',
        '-moz-user-select': 'none',
        '-khtml-user-select': 'none',
        '-webkit-user-select': 'none',
        '-ms-user-select': 'none',
        'user-select': 'none'
    },
}))(Badge);

const useStyles = makeStyles(theme => ({
    card: {
        backgroundColor: '#fff',
        width: '60px',
        height: '100px',
        marginTop: '10px',
        marginBottom: '10px',
        border: '1px solid #fff',
        borderRadius: '6px',
        cursor: 'pointer',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        '& .cardContent': {
            padding: '0px',
            cursor: 'pointer',
        }
    },
    cardSmall: {
        width: '50px',
        height: '70px',
        '& .cardText': {
            cursor: 'pointer',
            '-moz-user-select': 'none',
            '-khtml-user-select': 'none',
            '-webkit-user-select': 'none',
            '-ms-user-select': 'none',
            'user-select': 'none'
        },
        '& .cardBody': {
            textAlign: 'center',
            '& .cardBodyText': {
                fontSize: '21px',
                textAlign: 'center',
                color: '#666',
            },
            '& .cardBodyIcon': {
                textAlign: 'center',
                color: '#777',
                fontSize: '26px',
                marginBlockEnd: '0.1em',
                marginBlockStart: '0.1em',
                cursor: 'pointer',
            },
            '& .cardLoading': {
                width: '20px !important',
                height: '20px !important',
            }
        },
        '& .cardBadge': {
            fontSize: '12px',
            color: '#999',
            fontWeight: 500,
            cursor: 'pointer'
        }
    },
    cardMedium: {
        width: '100px',
        height: '140px',
        '& .cardText': {
            cursor: 'pointer',
            '-moz-user-select': 'none',
            '-khtml-user-select': 'none',
            '-webkit-user-select': 'none',
            '-ms-user-select': 'none',
            'user-select': 'none'
        },
        '& .cardBody': {
            textAlign: 'center',
            '& .cardBodyText': {
                fontSize: '45px',
                textAlign: 'center',
                color: '#666',
                fontWeight: 'bold'
            },
            '& .cardBodyIcon': {
                textAlign: 'center',
                color: '#777',
                fontSize: '45px',
                cursor: 'pointer',
                marginTop: '8px'
            },
            '& .cardLoading': {
                width: '40px !important',
                height: '40px !important',
            }
        },
        '& .cardBadge': {
            fontSize: '12px',
            color: '#999',
            fontWeight: 500,
            cursor: 'pointer'
        }
    },
    cardDisabled: {
        opacity: 0.8,
        cursor: 'default',
        '& .cardContent': {
            cursor: 'default',
        },
        '& .cardText': {
            cursor: 'default',
        },
        '& .cardBody': {
            '& .cardBodyIcon': {
                cursor: 'default',
            },
        },
        '& .cardBadge': {
            cursor: 'default'
        }
    },
    cardSelected: {
        marginTop: '0px',
        backgroundColor: '#41c300'
    },
    cardEmpty: {
        backgroundColor: '#fff',
        border: 'dashed 1px #999',
        cursor: 'default',
        '& .cardContent': {
            cursor: 'default',
        },
        '& .cardText': {
            cursor: 'default',
        },
        '& .cardBody': {
            '& .cardBodyIcon': {
                cursor: 'default',
            },
        },
        '& .cardBadge': {
            cursor: 'default'
        }
    },
    noCard: {
        backgroundColor: '#ffffff00',
        border: 'none',
        cursor: 'default',
        '& .cardContent': {
            cursor: 'default',
        },
        '& .cardText': {
            cursor: 'default',
        },
        '& .cardBody': {
            '& .cardBodyIcon': {
                cursor: 'default',
            },
        },
        '& .cardBadge': {
            cursor: 'default'
        }
    },
    cardBackside: {
        backgroundImage: `url(${bgCard})`,
        backgroundSize: 'cover',
        border: '1px solid #fff'
    }
}))

const CardPoker = (props) => {
    const {
        cardValue, cardText, CardIcon,
        selected, onSelect, size,
        players, loading, disabled, backside, noCard,
        backColor, frontColor
    } = props
    const classes = useStyles()

    const isEmpty = () => {
        return !cardText && !CardIcon
    }

    const handleClick = () => {
        if (!disabled && typeof onSelect === 'function') {
            onSelect(cardValue, cardText)
        }
    }

    return (
        <StyledBadge
            color="primary"
            invisible={!players}
            badgeContent={players}
            onClick={handleClick}
        >
            <Card
                className={
                    clsx(classes.card, {
                        [classes.cardSmall]: size === 'small',
                        [classes.cardMedium]: size === 'medium',
                        [classes.cardSelected]: selected,
                        [classes.cardEmpty]: isEmpty(),
                        [classes.noCard]: noCard,
                        [classes.cardDisabled]: disabled,
                        [classes.cardBackside]: backside
                    })
                }
                elevation={0}
                {...props}
                style={{
                    ...props.style,
                    backgroundColor: !selected && !backside && backColor ? backColor : '',
                    background: !selected && !backside && backColor ? `radial-gradient(circle, ${hexToHSL(backColor, 0, 8)} 5%, ${backColor} 74%)` : ''
                }}
            >
                <CardContent className='cardContent'>
                    {
                        !backside &&
                        <div className='cardBody'>
                            {
                                loading &&
                                <CircularProgress className='cardLoading' />
                            }
                            {
                                cardText && !CardIcon && !selected && !loading &&
                                <Typography className='cardBodyText cardText' style={{ color: frontColor }}>
                                    {cardText}
                                </Typography>
                            }
                            {
                                CardIcon && !selected && !loading &&
                                <CardIcon className='cardBodyIcon' style={{ color: frontColor }} />
                            }
                        </div>
                    }
                </CardContent>
            </Card>
        </StyledBadge>
    )
}

CardPoker.propTypes = {
    onSelect: PropTypes.func,
    selected: PropTypes.bool,
    cardValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    cardText: PropTypes.string,
    size: PropTypes.string,
    loading: PropTypes.bool,
    backside: PropTypes.bool,
    noCard: PropTypes.bool,
    CardIcon: PropTypes.element
}

CardPoker.defaultProps = {
    cardText: '',
    cardValue: '',
    size: 'small',
    loading: false,
    backside: false,
    noCard: false,
    CardIcon: null
}

export default CardPoker