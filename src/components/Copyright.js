import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

class Copyright extends React.Component {
    render() {
        return (
            <div style={{ display: 'none' }}>
                <Typography variant="body2" color="textSecondary" align="center" >
                    {'Copyright © '}
                    <Link color="inherit" href="" >
                        XTeam
                </Link> {' '}
                    {new Date().getFullYear()}
                    {'.'}
                </Typography >
            </div>
        )
    }
}

export default Copyright