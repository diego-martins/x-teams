import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Dialog,
    Container,
    Slide,
    TextField
} from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import TeamService from '../services/teamService'
import MemberService from '../services/memberService'
import MyButton from '../components/MyButton'
import { EMOTION_HAPPY, ATTENTION_GREEN } from '../constants'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import AuthService from '../services/authService'
import { useTranslation } from 'react-i18next'
import { normalize } from '../utils/textUtils'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    rootPaper: {
        padding: theme.spacing(3, 2),
    },
    rootContainer: {
    },
    rootList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        flexGrow: 1,
    },
    appBarModal: {
        position: 'relative',
    },
    titleModal: {
        marginLeft: theme.spacing(0),
        flex: 1,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    toobarRightIcon: {
        marginRight: theme.spacing(1),
    },
    paperCurrentMember: {
        padding: '10px',
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        backgroundColor: '#3F51B5',
        color: '#fff'
    },
    bigAvatar: {
        width: 50,
        height: 50,
        margin: '5px'
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonProgress: {
        color: theme.palette.secondary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    }
}))

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const CreateTeamDialog = (props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const { data, open, onClose } = props
    const [loading, setLoading] = React.useState(false)
    const [fields, setFields] = React.useState({})
    const { register, handleSubmit, errors } = useForm()
    const { showDialog } = React.useContext(DialogContext)
    const user = AuthService.getCurrentUser()

    React.useEffect(() => {
        setFields(data || {})
    }, [data])

    const handleClose = () => onClose(null)

    const onCloseCreateTeam = () => {
        onClose(fields)
    }

    const createTeam = (model) => {
        setLoading(true)
        TeamService.existsBy(user.id, normalize(model.name), normalize(model.company))
            .then(exists => {
                if (exists) {
                    showDialog({ message: t('error_team_already_exists') })
                    setLoading(false)
                    return
                }

                return TeamService.create(user.id, model.name, model.company)
                    .then(createdTeam => {
                        return MemberService.add(createdTeam.id, user.id, user.name, EMOTION_HAPPY, ATTENTION_GREEN, true)
                            .then(() => {
                                setLoading(false)
                                onCloseCreateTeam(true)
                            })
                    })
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    const editTeam = (model) => {
        setLoading(true)
        TeamService.existsBy(user.id, normalize(model.name), normalize(model.company), model.id)
            .then(exists => {
                if (exists) {
                    showDialog({ message: t('error_team_already_exists') })
                    setLoading(false)
                    return
                }

                return TeamService.update(model.id, model.name, model.company)
                    .then(() => {
                        setLoading(false)
                        onCloseCreateTeam(true)
                    })
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    const onSubmit = (data) => {
        if (data.id) {
            editTeam(data)
            return
        }
        createTeam(data)
    }

    return (
        <Dialog fullScreen open={open} TransitionComponent={Transition}>
            <AppBar className={classes.appBarModal}>
                <Toolbar position='static'>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.titleModal}>
                        {fields.id ? t('title_edit_team') : t('title_create_team')}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container component="main" maxWidth="xs">

                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>

                    <input
                        type="hidden"
                        name="id"
                        defaultValue={fields.id}
                        ref={register}
                    />

                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        defaultValue={fields.name}
                        id="name"
                        label={t('hint_name')}
                        name="name"
                        autoComplete="name"
                        autoFocus
                        inputProps={{ maxLength: "30" }}
                        error={!!errors.name}
                        helperText={errors.name ? errors.name.message : ''}
                        inputRef={register({
                            required: t('error_required_field')
                        })}
                    />

                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        defaultValue={fields.company}
                        name="company"
                        label={t('hint_company')}
                        id="company"
                        autoComplete="company"
                        style={{ marginBottom: '24px' }}
                        inputProps={{ maxLength: "30" }}
                        error={!!errors.company}
                        helperText={errors.company ? errors.company.message : ''}
                        inputRef={register({
                            required: t('error_required_field')
                        })}
                    />

                    <MyButton
                        type="submit"
                        fullWidth
                        loading={loading}
                        variant="contained"
                        size="large"
                        color="primary"
                    >
                        {t('btn_save')}
                    </MyButton>

                </form>
            </Container>
        </Dialog>
    )
}

CreateTeamDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    data: PropTypes.object
}

export default CreateTeamDialog
