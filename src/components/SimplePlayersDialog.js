import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
    List,
    Divider,
    ListItem,
    ListItemAvatar,
    ListItemText,
    DialogTitle,
    Dialog,
    Avatar
} from '@material-ui/core'
import { getInitialUserName } from '../utils/userUtils'
import { stringToColor } from '../utils/colorUtils'

const useStyles = makeStyles({
    dialogTitle: {
        padding: '16px'
    }
})

const SimplePlayersDialog = (props) => {
    const classes = useStyles()
    const { title, players, open, onClose } = props

    const handleClose = () => {
        onClose()
    }

    return (
        <Dialog scroll='paper' onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
            <DialogTitle className={classes.dialogTitle} id="simple-dialog-title">{title}</DialogTitle>
            <Divider />
            <List>
                {players.map(player => {
                    const color = stringToColor(player.name)
                    return (
                        <ListItem key={player.id}>
                            <ListItemAvatar>
                                <Avatar style={{ backgroundColor: color.back, color: color.front }}>
                                    {getInitialUserName(player.name)}
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={player.name} />
                        </ListItem>
                    )
                })}
            </List>
        </Dialog>
    )
}

SimplePlayersDialog.propTypes = {
    title: PropTypes.string.isRequired,
    players: PropTypes.array.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired
}

export default SimplePlayersDialog