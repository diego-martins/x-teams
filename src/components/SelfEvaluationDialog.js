import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Dialog,
    Container,
    Slide,
    Grid,
    Avatar,
    Badge,
    Divider
} from '@material-ui/core'
import { Close as CloseIcon, Check as CheckIcon } from '@material-ui/icons'
import { getEmotionIcon } from '../utils/emotionUtils'
import MemberService from '../services/memberService'
import MyButton from '../components/MyButton'
import * as emotionUtils from '../utils/emotionUtils'
import { DialogContext } from '../contexts/DialogContext'
import AuthService from '../services/authService'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    rootPaper: {
        padding: theme.spacing(3, 2),
    },
    rootContainer: {
    },
    rootList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        flexGrow: 1,
    },
    appBarModal: {
        position: 'relative',
    },
    titleModal: {
        marginLeft: theme.spacing(0),
        flex: 1,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    toobarRightIcon: {
        marginRight: theme.spacing(1),
    },
    paperCurrentMember: {
        padding: '10px',
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        backgroundColor: '#3F51B5',
        color: '#fff'
    },
    bigAvatar: {
        width: 50,
        height: 50,
        margin: '5px',
        border: `1px solid #333`
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonProgress: {
        color: theme.palette.secondary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    }
}))

const SmallAvatar = withStyles(theme => ({
    root: {
        width: 30,
        height: 30,
        border: `2px solid #fff`,
        background: theme.palette.secondary.main
    },
}))(Avatar)

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const SelfEvaluationDialog = (props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const { teamId, open, onClose, emotionValue, attentionValue } = props
    const [selectedEmotion, setSelectedEmotion] = React.useState({})
    const [selectedAttention, setSelectedAttention] = React.useState({})
    const [loading, setLoading] = React.useState(false)
    const { showDialog } = React.useContext(DialogContext)
    const user = AuthService.getCurrentUser()

    React.useEffect(() => {
        setSelectedEmotion(emotionUtils.getEmotion(emotionValue) || {})
        setSelectedAttention(emotionUtils.getAttention(attentionValue) || {})
    }, [attentionValue, emotionValue])

    const handleClose = () => onClose(null)

    const handleSave = () => {
        if (loading) {
            return
        }

        if (selectedEmotion.id === emotionValue &&
            selectedAttention.id === attentionValue) {
            onClose(null)
        }

        const data = {
            name: user.name,
            emotion: selectedEmotion.id,
            attention: selectedAttention.id
        }
        setLoading(true)
        MemberService.update(teamId, user.id, data)
            .then(() => onClose(data))
            .catch(err => showDialog({ message: err.message }))
            .finally(() => setLoading(false))

    }

    return (
        <Dialog fullScreen open={open} TransitionComponent={Transition}>
            <AppBar className={classes.appBarModal}>
                <Toolbar position='static'>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.titleModal}>
                        {t('title_self_evaluation')}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container component="main" maxWidth="xs">
                <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{ minHeight: '90vh' }}
                >
                    <Grid item>

                        <Grid container>
                            <Grid item xs={6}>
                                <Typography variant="subtitle1" style={{ fontWeight: 500 }} align="left">
                                    {t('evaluation')}
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography variant="subtitle1" align="right">
                                    {selectedEmotion.name ? t(`emotion_${selectedEmotion.name}`) : ''}
                                </Typography>
                            </Grid>
                        </Grid>

                        <Divider />

                        <Grid container
                            style={{ marginTop: '16px', marginBottom: '24px' }}
                            alignItems="center"
                            justify="center">
                            {emotionUtils.getEmotions().map(item => (
                                <Grid item key={item.id}>
                                    <Badge
                                        invisible={selectedEmotion.id !== item.id}
                                        overlap="circle"
                                        anchorOrigin={{
                                            vertical: 'bottom',
                                            horizontal: 'right',
                                        }}
                                        badgeContent={<SmallAvatar><CheckIcon /></SmallAvatar>}
                                    >
                                        <Avatar
                                            src={getEmotionIcon(
                                                item.id,
                                                selectedEmotion.id === item.id ? selectedAttention.id : 0
                                            )}
                                            onClick={() => setSelectedEmotion(item)}
                                            className={classes.bigAvatar}
                                        />
                                    </Badge>
                                </Grid>
                            ))}
                        </Grid>

                        <Grid container>
                            <Grid item xs={6}>
                                <Typography variant="subtitle1" align="left" style={{ fontWeight: 500 }}>
                                    {t('availability')}
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography variant="subtitle1" align="right">
                                    {selectedAttention.name ? t(`attention_${selectedAttention.name}`) : ''}
                                </Typography>
                            </Grid>
                        </Grid>

                        <Divider />

                        <Grid container
                            style={{ marginTop: '16px' }}
                            alignItems="center"
                            justify="center">
                            {emotionUtils.getAttentions().map(item => (
                                <Grid item key={item.id}>
                                    <Badge
                                        invisible={selectedAttention.id !== item.id}
                                        overlap="circle"
                                        anchorOrigin={{
                                            vertical: 'bottom',
                                            horizontal: 'right',
                                        }}
                                        badgeContent={<SmallAvatar><CheckIcon /></SmallAvatar>}
                                    >
                                        <Avatar
                                            style={emotionUtils.getAttentionStyleColor(item.id)}
                                            className={classes.bigAvatar}
                                            onClick={() => setSelectedAttention(item)}
                                        />
                                    </Badge>
                                </Grid>
                            ))}
                        </Grid>

                        <Grid container
                            style={{ marginTop: '32px' }}
                            alignItems="center"
                            justify="center">
                            <Grid item xs={6}>
                                <MyButton
                                    onClick={handleSave}
                                    loading={loading}
                                    type="button"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    size="large">
                                    {t('btn_save')}
                                </MyButton>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        </Dialog>
    )
}

SelfEvaluationDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    emotionValue: PropTypes.number.isRequired,
    attentionValue: PropTypes.number.isRequired,
    teamId: PropTypes.string.isRequired
}

export default SelfEvaluationDialog