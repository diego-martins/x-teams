import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import firebase from '../utils/firebaseUtils'
import ErrorBoundary from '../components/ErrorBoundary'

const PrivateRoute = ({ component: Component, needAuth, redirectAutenticatedTo, ...rest }) => {
    const user = firebase.auth().currentUser
    return (
        <Route {...rest} render={props => (
            (needAuth && !user) || (!needAuth && user && redirectAutenticatedTo)
                ?
                <Redirect to={{
                    pathname: redirectAutenticatedTo || '/sign-in',
                    state: { from: props.location }
                }} />
                :
                <ErrorBoundary>
                    <Component {...props} />
                </ErrorBoundary>
        )} />
    )
}

export default PrivateRoute