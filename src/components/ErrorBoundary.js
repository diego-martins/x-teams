import React, { Component } from 'react'
import { withTranslation } from 'react-i18next'

class ErrorBoundary extends Component {
    constructor(props) {
        super(props)
        this.state = { error: null, errorInfo: null }
    }

    // For this example we'll just use componentDidCatch, this is only 
    // here to show you what this method would look like.
    // static getDerivedStateFromProps(error){
    // return { error: true }
    // }

    componentDidCatch(error, info) {
        this.setState({ error: error, errorInfo: info })
        if (error) {
            console.error('ErrorBoundary', error)
        }
    }

    render() {
        const { t } = this.props
        if (this.state.errorInfo) {
            return (
                <div className="error-boundary">
                    <h3 className="title">{t('error_something_title')}</h3>
                    <p className="message">{t('error_something_message')}</p>
                    <a className="btn-retry" href='/'>{t('btn_retry')}</a>
                </div>
            )
        }
        return this.props.children
    }
}

export default withTranslation()(ErrorBoundary)