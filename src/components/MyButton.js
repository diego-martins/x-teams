import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { Button, CircularProgress } from '@material-ui/core'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
    wrapper: {
        position: 'relative',
    },
    buttonProgress: {
        color: theme.palette.secondary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    rounded: {
        borderRadius: '24px'
    },
    sizeMicro: {
        fontSize: '0.6125rem'
    }
}))

const MyButton = (props) => {
    const { disabled, loading, classes, children, sizeMicro, rounded, ...rest } = props
    const myClasses = useStyles()

    return (
        <div className={clsx(myClasses.wrapper, classes)}>
            <Button
                className={clsx(
                    {
                        [myClasses.rounded]: rounded,
                        [myClasses.sizeMicro]: sizeMicro
                    }
                )}
                disabled={disabled || loading}
                {...rest}
            >
                {children}
            </Button>
            {
                loading &&
                <CircularProgress size={24} className={myClasses.buttonProgress} />
            }
        </div>
    )
}

MyButton.propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    rounded: PropTypes.bool,
    classes: PropTypes.object,
    children: PropTypes.string
}

MyButton.defaultProps = {
    disabled: false,
    loading: false,
    rounded: false,
    classes: {},
    children: null
}

export default MyButton