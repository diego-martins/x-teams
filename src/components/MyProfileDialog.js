import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import {
    Dialog,
    DialogContent,
    Grid,
    Typography
} from '@material-ui/core'
import { blue } from '@material-ui/core/colors'
import MyButton from '../components/MyButton'
import AuthService from '../services/authService'
import { getInitialUserName } from '../utils/userUtils'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'
import { stringToColor } from '../utils/colorUtils'

const useStyles = makeStyles(theme => ({
    dialog: {
        '& .MuiDialogContent-root': {
            paddingTop: '20px',
            paddingBottom: '20px'
        }
    },
    avatar: {
        backgroundColor: blue[100],
        color: blue[600],
        width: theme.spacing(9),
        height: theme.spacing(9),
        fontSize: '2.5em',
        margin: 'auto'
    },
    submit: {
        width: '100%'
    },
    centered: {
        textAlign: 'center',
        margin: 'auto'
    },
    overtext: {
        textAlign: 'center',
        margin: 'auto',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
    }
}))

const MyProfileDialog = (props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const { user, open, onClose } = props
    const [loadingSignOut, setLoadingSignOut] = React.useState(false)
    const { showDialog, hideDialog } = React.useContext(DialogContext)

    const handleClose = () => {
        onClose()
    }

    const onSignOut = () => {
        showDialog({
            message: t('confirm_sign_out'),
            onOk: () => {
                setLoadingSignOut(true)
                AuthService.signOut()
                    .then(() => {
                        setLoadingSignOut(false)
                        onClose()
                    })
                    .catch(err => {
                        setLoadingSignOut(false)
                        console.error(err)
                    })
                hideDialog()
            },
            onCancel: () => hideDialog()
        })
    }

    const color = stringToColor(user.name)

    return (
        <Dialog maxWidth="xs" className={classes.dialog} onClose={handleClose} aria-labelledby="my-profile-dialog" open={open}>
            <DialogContent>
                <Grid
                    container
                    spacing={1}
                >
                    <Grid item xs={12}>
                        <Avatar className={classes.avatar} style={{ backgroundColor: color.back, color: color.front }}>
                            {getInitialUserName(user.name)}
                        </Avatar>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h6" className={classes.centered}>
                            {user.name}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1" className={classes.overtext}>
                            {user.email}
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <MyButton
                            variant="contained"
                            size="large"
                            color="primary"
                            type='submit'
                            onClick={onSignOut}
                            loading={loadingSignOut}
                            disabled={loadingSignOut}
                            className={classes.submit}
                        >
                            {t('btn_sign_out')}
                        </MyButton>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="caption" display="block" className={classes.centered}>
                            v{process.env.REACT_APP_VERSION}
                        </Typography>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
}

MyProfileDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
}

export default MyProfileDialog