import React from 'react'
import PropTypes from 'prop-types'
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button
} from '@material-ui/core'
import { useTranslation } from 'react-i18next'

const MyDialog = (props) => {
    const {
        open, title, message, onOk, onCancel, okText, cancelText
    } = props
    const { t } = useTranslation()
    const handleOk = () => { if (onOk) onOk() }
    const handleCancel = () => { if (onCancel) onCancel() }

    return (
        <Dialog
            open={open}
            disableBackdropClick={!!onCancel}
            disableEscapeKeyDown={!!onCancel}
        >
            {
                title &&
                <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            }
            <DialogContent>
                <DialogContentText id="alert-dialog-message">
                    {message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                {
                    onCancel &&
                    <Button onClick={handleCancel} color="primary">
                        {cancelText || t('btn_cancel')}
                    </Button>
                }
                <Button onClick={handleOk} color="primary">
                    {okText || t('btn_ok')}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

MyDialog.propTypes = {
    title: PropTypes.string,
    message: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    onOk: PropTypes.func.isRequired,
    onCancel: PropTypes.func,
    okText: PropTypes.string,
    okCancel: PropTypes.string
}

export default MyDialog