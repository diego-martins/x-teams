import React from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import MyButton from '../components/MyButton'

const EmptyList = (props) => {
    const { title, subtitle, btnText, onClick } = props
    const displayTitle = title || 'Items not found'
    const displaySubtitle = subtitle || ''
    return (
        <Grid container
            spacing={0}
            direction="column"
            alignItems="center"
            justify="center"
            style={{ minHeight: '90vh' }}>

            <Grid item>
                <Typography variant="h6" style={{ textAlign: 'center' }}>{displayTitle}</Typography>
                {
                    displaySubtitle &&
                    <Typography variant="body1">{displaySubtitle}</Typography>
                }

                {
                    btnText &&
                    <MyButton onClick={onClick} color="primary" variant="contained" size="large" style={{ margin: '10px' }}>{btnText}</MyButton>
                }
            </Grid>
        </Grid>
    )
}

EmptyList.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    btnText: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default EmptyList