import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Dialog,
    Container,
    Slide,
    TextField
} from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import UserService from '../services/userService'
import MemberService from '../services/memberService'
import MyButton from '../components/MyButton'
import { EMOTION_HAPPY, ATTENTION_GREEN } from '../constants'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    rootPaper: {
        padding: theme.spacing(3, 2),
    },
    rootContainer: {
    },
    rootList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        flexGrow: 1,
    },
    appBarModal: {
        position: 'relative',
    },
    titleModal: {
        marginLeft: theme.spacing(0),
        flex: 1,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    toobarRightIcon: {
        marginRight: theme.spacing(1),
    },
    paperCurrentMember: {
        padding: '10px',
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        backgroundColor: '#3F51B5',
        color: '#fff'
    },
    bigAvatar: {
        width: 50,
        height: 50,
        margin: '5px'
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonProgress: {
        color: theme.palette.secondary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    }
}))

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const AddMemberDialog = (props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const { teamId, open, onClose } = props
    const [loading, setLoading] = React.useState(false)
    const { register, handleSubmit, errors } = useForm()
    const { showDialog } = React.useContext(DialogContext)

    const handleClose = () => onClose(null)

    const onCloseAddMember = (data) => {
        onClose(data)
    }

    const onSubmit = (data) => {
        setLoading(true)
        UserService.getByEmail(data.email.toLowerCase())
            .then(user => {
                if (!user) {
                    showDialog({ message: t('error_user_not_found') })
                    setLoading(false)
                    return
                }

                return MemberService.getById(teamId, user.id)
                    .then(memberResult => {
                        if (memberResult && !memberResult.deletedAt) {
                            showDialog({ message: t('error_member_already_exists') })
                            setLoading(false)
                            return
                        }

                        if (memberResult && memberResult.deletedAt) {
                            return MemberService.undoDelete(teamId, user.id, `${user.firstName} ${user.lastName}`)
                                .then(() => {
                                    setLoading(false)
                                    onCloseAddMember(data)
                                })
                        }

                        return MemberService.add(teamId, user.id, `${user.firstName} ${user.lastName}`, EMOTION_HAPPY, ATTENTION_GREEN, false)
                            .then(() => {
                                setLoading(false)
                                onCloseAddMember(data)
                            })
                    })
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    return (
        <Dialog fullScreen open={open} TransitionComponent={Transition}>
            <AppBar className={classes.appBarModal}>
                <Toolbar position='static'>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.titleModal}>
                        {t('title_add_member')}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container component="main" maxWidth="xs">

                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>

                    <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="email"
                        label={t('hint_email')}
                        id="email"
                        autoComplete="email"
                        style={{ marginBottom: '24px' }}
                        inputProps={{ maxLength: "100" }}
                        error={!!errors.email}
                        helperText={errors.email ? errors.email.message : ''}
                        onChange={e => {
                            if (e.target.value) {
                                e.target.value = e.target.value.toLowerCase();
                            }
                        }}
                        inputRef={register({
                            required: t('error_required_field'),
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                message: t('error_invalid_email')
                            }
                        })}
                    />

                    <MyButton
                        type="submit"
                        fullWidth
                        loading={loading}
                        variant="contained"
                        size="large"
                        color="primary"
                    >
                        {t('btn_save')}
                    </MyButton>

                </form>
            </Container>
        </Dialog>
    )
}

AddMemberDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    teamId: PropTypes.string.isRequired
}

export default AddMemberDialog
