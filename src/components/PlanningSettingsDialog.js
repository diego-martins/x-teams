import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Dialog,
    Container,
    Slide,
    Select,
    FormControl,
    InputLabel,
    FormHelperText
} from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import PlanningPokerService from '../services/planningPokerService'
import MyButton from '../components/MyButton'
import useForm from 'react-hook-form'
import { DialogContext } from '../contexts/DialogContext'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    rootPaper: {
        padding: theme.spacing(3, 2),
    },
    rootContainer: {
    },
    rootList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        flexGrow: 1,
    },
    appBarModal: {
        position: 'relative',
    },
    titleModal: {
        marginLeft: theme.spacing(0),
        flex: 1,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    toobarRightIcon: {
        marginRight: theme.spacing(1),
    },
    paperCurrentMember: {
        padding: '10px',
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        backgroundColor: '#3F51B5',
        color: '#fff'
    },
    bigAvatar: {
        width: 50,
        height: 50,
        margin: '5px'
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonProgress: {
        color: theme.palette.secondary.main,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 250,
        marginBottom: theme.spacing(2)
    },
    selectEmpty: {
        marginTop: theme.spacing(0),
    },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />
})

const PlanningSettingsDialog = (props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const { teamId, open, onClose } = props
    const [loading, setLoading] = React.useState(false)
    const [fields, setFields] = React.useState({})
    const { register, handleSubmit, setValue } = useForm()
    const { showDialog } = React.useContext(DialogContext)
    const [cardSequenceValue, setCardSequenceValue] = React.useState('standard')

    React.useEffect(() => {
        if (open) {
            register({ name: 'cardSequence' })
            PlanningPokerService.getById(teamId)
                .then(result => {
                    if (result) {
                        setCardSequenceValue(result.cardSequence || 'standard')
                    }
                })
        }
    }, [open, register, teamId])

    const handleCardSequenceChange = selectedOption => {
        setValue("cardSequence", selectedOption.target.value)
        setCardSequenceValue(selectedOption.target.value)
    }

    const handleClose = () => onClose(null)

    const clearFields = () => {
        setFields({})
    }

    const onCloseDialog = () => {
        onClose(fields)
        clearFields()
    }

    const save = (model) => {
        setLoading(true)
        PlanningPokerService.update(teamId, { cardSequence: model.cardSequence })
            .then(() => {
                setLoading(false)
                onCloseDialog()
            })
            .catch(err => {
                setLoading(false)
                showDialog({ message: err.message })
            })
    }

    const onSubmit = (data) => {
        save(data)
    }

    const getSampleSequences = (cardSequence) => {
        switch (cardSequence) {
            case 'natural':
                return '0, 1, 2, 3, 4, 5...'
            case 't-shirt':
                return 'XS, S, M, L, XL, XXL'
            case 'fibonacci':
                return '0, 1, 2, 3, 5, 8...'
            case 'standard':
            default:
                return '0, 0.5, 1, 2, 3, 5...'
        }
    }

    return (
        <Dialog fullScreen open={open} TransitionComponent={Transition}>
            <AppBar className={classes.appBarModal}>
                <Toolbar position='static'>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.titleModal}>
                        {t('title_planning_settings')}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Container component="main" maxWidth="xs">

                <Typography variant="h6" style={{ marginTop: '20px' }}>
                    Planning poker
                </Typography>

                <form className={classes.form} onSubmit={handleSubmit(onSubmit)} noValidate>

                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="card-sequence">{t('hint_card_sequence')}</InputLabel>
                        <Select
                            native
                            name="cardSequence"
                            value={cardSequenceValue}
                            labelId="card-sequence"
                            id="card-sequence"
                            onChange={handleCardSequenceChange}
                        >
                            <option value="standard">{t('standard')}</option>
                            <option value="fibonacci">Fibonacci</option>
                            <option value="t-shirt">T-Shirt</option>
                            <option value="natural">Natural</option>
                        </Select>
                        <FormHelperText>{getSampleSequences(cardSequenceValue)}</FormHelperText>
                    </FormControl>

                    <MyButton
                        type="submit"
                        fullWidth
                        loading={loading}
                        variant="contained"
                        size="large"
                        color="primary"
                    >
                        {t('btn_save')}
                    </MyButton>

                </form>
            </Container>
        </Dialog>
    )
}

PlanningSettingsDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    teamId: PropTypes.string.isRequired
}

export default PlanningSettingsDialog
