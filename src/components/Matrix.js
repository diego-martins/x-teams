import React from 'react'
// import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
    Grid,
    Typography,
    Button
} from '@material-ui/core'
import clsx from 'clsx'
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    button: {
        textTransform: 'lowercase',
        color: '#666'
    },
    root: {
        position: 'relative'
    },
    item: {
        // minHeight: '50px'
    },
    leftBorder: {
        borderLeft: '1px solid #666'
    },
    bottomBorder: {
        borderBottom: '1px solid #666'
    },
    itemText: {
        textAlign: 'center',
        cursor: 'default',
        lineHeight: '2',
        '-moz-user-select': 'none',
        '-khtml-user-select': 'none',
        '-webkit-user-select': 'none',
        '-ms-user-select': 'none',
        'user-select': 'none'
    },
    itemLabelText: {
        fontSize: '12px'
    },
    levelTextY: {
        lineHeight: '3.4',
        textAlign: 'right',
        cursor: 'default',
        '-moz-user-select': 'none',
        '-khtml-user-select': 'none',
        '-webkit-user-select': 'none',
        '-ms-user-select': 'none',
        'user-select': 'none',
        'marginRight': '10px'
    },
    levelTextX: {
        lineHeight: '3.4',
        textAlign: 'center',
        cursor: 'default',
        '-moz-user-select': 'none',
        '-khtml-user-select': 'none',
        '-webkit-user-select': 'none',
        '-ms-user-select': 'none',
        'user-select': 'none'
    },
    itemSelected: {
        backgroundColor: '#4caf50',
        '& .MuiTypography-root': {
            color: '#fff'
        }
    },
    axisX: {
        textAlign: 'left',
        bottom: '-26px',
        position: 'absolute',
        left: '-16px'
    },
    axisY: {
        position: 'absolute',
        transform: 'rotate(-90deg)',
        transformOrigin: 'left bottom',
        bottom: '0px',
        left: '-10px'
    }
}))

const Matrix = (props) => {
    const { selectedId, onClick, disabled, sequence, ...rest } = props
    const classes = useStyles()
    const { t } = useTranslation()

    const handleClickItem = (item) => {
        if (!disabled && !item.label && onClick) onClick({
            id: item.id,
            value: parseInt(item.text),
            text: item.text
        })
    }

    const items = [
        {
            row: 0,
            cols: [
                { id: -1, col: 0, text: 'XL', label: true },
                { id: 1, col: 1, text: '8' },
                { id: 2, col: 2, text: '13' },
                { id: 3, col: 3, text: sequence === 'fibonacci' ? '21' : '20' },
                { id: 4, col: 4, text: sequence === 'fibonacci' ? '34' : '40' },
                { id: 5, col: 5, text: sequence === 'fibonacci' ? '55' : '100' }
            ]
        },
        {
            row: 1,
            cols: [
                { id: -6, col: 0, text: 'L', label: true },
                { id: 6, col: 1, text: '5' },
                { id: 7, col: 2, text: '8' },
                { id: 8, col: 3, text: '13' },
                { id: 9, col: 4, text: sequence === 'fibonacci' ? '21' : '20' },
                { id: 10, col: 5, text: sequence === 'fibonacci' ? '34' : '40' }
            ]
        },
        {
            row: 2,
            cols: [
                { id: -11, col: 0, text: 'M', label: true },
                { id: 11, col: 1, text: '3' },
                { id: 12, col: 2, text: '5' },
                { id: 13, col: 3, text: '8' },
                { id: 14, col: 4, text: '13' },
                { id: 15, col: 5, text: sequence === 'fibonacci' ? '21' : '20' }
            ]
        },
        {
            row: 3,
            cols: [
                { id: -16, col: 0, text: 'S', label: true },
                { id: 16, col: 1, text: '2' },
                { id: 17, col: 2, text: '3' },
                { id: 18, col: 3, text: '5' },
                { id: 19, col: 4, text: '8' },
                { id: 20, col: 5, text: '13' }
            ]
        },
        {
            row: 4,
            cols: [
                { id: -21, col: 0, text: 'XS', label: true },
                { id: 21, col: 1, text: '1' },
                { id: 22, col: 2, text: '2' },
                { id: 23, col: 3, text: '3' },
                { id: 24, col: 4, text: '5' },
                { id: 25, col: 5, text: '8' }
            ]
        },
        {
            row: 5,
            cols: [
                { id: -26, col: 0, text: '', label: true },
                { id: 26, col: 1, text: 'XS', label: true },
                { id: 27, col: 2, text: 'S', label: true },
                { id: 28, col: 3, text: 'M', label: true },
                { id: 29, col: 4, text: 'L', label: true },
                { id: 30, col: 5, text: 'XL', label: true }
            ]
        }
    ]

    return (
        <div className={classes.root} {...rest}>
            {
                items.map(row => {
                    return (
                        <Grid
                            container
                            spacing={0}
                            alignItems="center"
                            justify="center"
                            className={classes.row}
                        >
                            {
                                row.cols.map(col => {
                                    return (
                                        <Grid key={col.id} item xs={col.col === 0 ? 1 : 2}
                                            className={clsx(
                                                {
                                                    [classes.leftBorder]: !col.label,
                                                    [classes.bottomBorder]: !col.label,
                                                    [classes.itemSelected]: !col.label && selectedId === col.id
                                                })}
                                            onClick={() => handleClickItem(col)}
                                        >
                                            <div className={clsx(classes.item, {
                                                [classes.itemLabel]: col.label
                                            })}>
                                                <Typography className={clsx(classes.itemText, {
                                                    [classes.itemLabelText]: col.label
                                                })} variant="h6" color='textSecondary'>
                                                    {`${col.text}`}
                                                </Typography>
                                            </div>
                                        </Grid>
                                    )
                                })
                            }
                        </Grid>
                    )
                })
            }

            <Grid
                container
                spacing={0}
                alignItems="center"
                justify="center"
                className={classes.row}
            >

            </Grid>

            <Grid
                container
                spacing={0}
                alignItems="center"
                justify="center"
                className={classes.row}
            >
                <Grid item xs={10} style={{ position: 'relative' }}>
                    <div className={classes.axisY}>
                        <Button
                            size="small"
                            disabled={true}
                            endIcon={<ArrowRightAltIcon />}
                            style={{ textTransform: 'lowercase', color: '#666' }}
                        >
                            {t('axis_y').toLowerCase()}
                        </Button>
                    </div>
                    <div className={classes.axisX}>
                        <Button
                            size="small"
                            disabled={true}
                            endIcon={<ArrowRightAltIcon />}
                            style={{ textTransform: 'lowercase', color: '#666' }}
                        >
                            {t('axis_x').toLowerCase()}
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

Matrix.propTypes = {
}

Matrix.defaultProps = {
}

export default Matrix