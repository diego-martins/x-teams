import firebase from '../utils/firebaseUtils'

export default class PlanningPokerService {
    static getById = (teamId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .get()
                .then(snap => {
                    if (snap.exists) {
                        const doc = { id: snap.id, ...snap.data() }
                        return resolve(doc)
                    }
                    resolve(null)
                })
                .catch(err => reject(err))
        })
    }

    static getPlayerById = (teamId, memberId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players').doc(memberId)
                .get()
                .then(snap => {
                    if (snap.exists) {
                        const doc = { id: snap.id, ...snap.data() }
                        return resolve(doc)
                    }
                    resolve(null)
                })
                .catch(err => reject(err))
        })
    }

    static start = (teamId, playerId, name, players, startAt, finishAt) => {
        return new Promise((resolve, reject) => {
            let batch = firebase.firestore().batch()
            players.forEach(player => {
                let playerRef = firebase.firestore()
                    .collection('planningPoker').doc(teamId)
                    .collection('players').doc(player)
                batch.set(playerRef, {
                    cardValue: null,
                    modifiedAt: firebase.firestore.Timestamp.fromMillis(startAt),
                    startedAt: null,
                    finishedAt: null,
                    canceledAt: null
                }, { merge: true })
            })

            let playerRef = firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players').doc(playerId)
            batch.set(playerRef, {
                name,
                cardValue: null,
                modifiedAt: firebase.firestore.Timestamp.fromMillis(startAt),
                startedAt: firebase.firestore.Timestamp.fromMillis(startAt),
                finishedAt: firebase.firestore.Timestamp.fromMillis(finishAt),
                canceledAt: null
            }, { merge: true })

            return batch.commit()
                .then(() => {
                    resolve()
                })
                .catch(err => reject(err))
        })
    }

    static cancel = (teamId, playerId, players, canceledAt) => {
        return new Promise((resolve, reject) => {
            let batch = firebase.firestore().batch()
            players.forEach(player => {
                let playersRef = firebase.firestore()
                    .collection('planningPoker').doc(teamId)
                    .collection('players').doc(player)
                batch.set(playersRef, {
                    cardValue: null,
                    modifiedAt: firebase.firestore.Timestamp.fromMillis(canceledAt),
                    canceledAt: null
                }, { merge: true })
            })

            let playerRef = firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players').doc(playerId)
            batch.set(playerRef, {
                cardValue: null,
                modifiedAt: firebase.firestore.Timestamp.fromMillis(canceledAt),
                canceledAt: firebase.firestore.Timestamp.fromMillis(canceledAt)
            }, { merge: true })

            return batch.commit()
                .then(() => {
                    resolve()
                })
                .catch(err => reject(err))
        })
    }

    static update = (teamId, data) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .set({
                    ...data,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, { merge: true })
                .then(() => resolve(true))
                .catch(err => reject(err))
        })
    }

    static observerAllPlayers = (teamId) => {
        return firebase.firestore()
            .collection('planningPoker').doc(teamId)
            .collection('players').orderBy('name')
    }

    static updatePlayer = (teamId, memberId, data) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players')
                .doc(memberId)
                .update({
                    ...data,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => resolve(true))
                .catch(err => reject(err))
        })
    }

    static updatePlayerCardValue = (teamId, memberId, name, cardValue) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players')
                .doc(memberId)
                .set({
                    name,
                    cardValue,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                }, { merge: true })
                .then(() => resolve(true))
                .catch(err => reject(err))
        })
    }

    static undoPlayerDelete = (teamId, memberId, name) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players')
                .doc(memberId)
                .update({
                    name,
                    deletedAt: null,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    const doc = {
                        memberIds: firebase.firestore.FieldValue.arrayUnion(memberId)
                    }

                    return firebase.firestore()
                        .collection('teams')
                        .doc(teamId)
                        .update(doc)
                        .then(() => resolve(true))
                })
                .catch(err => reject(err))
        })
    }

    static deletePlayer = (teamId, memberId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('planningPoker').doc(teamId)
                .collection('players')
                .doc(memberId)
                .update({
                    admin: false,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
                    deletedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    const doc = {
                        memberIds: firebase.firestore.FieldValue.arrayRemove(memberId),
                        adminIds: firebase.firestore.FieldValue.arrayRemove(memberId)
                    }
                    return firebase.firestore()
                        .collection('teams')
                        .doc(teamId)
                        .update(doc)
                        .then(() => resolve(true))
                })
                .catch(err => reject(err))
        })
    }
}