import firebase from '../utils/firebaseUtils'

export default class AuthService {
    static getCurrentUser = () => {
        let user = firebase.auth().currentUser
        if (user) {
            user.id = user.uid
            user.name = user.displayName || ''
            const parts = user.name.split(' ')
            if (parts.length) {
                user.firstName = parts[0]
                if (parts.length > 1) {
                    user.lastName = parts[1]
                }
            }
        }
        return user
    }

    static signUp = (email, password) => {
        return new Promise((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(result => {
                    resolve(result ? result.user : null)
                })
                .catch(err => reject(err))
        })
    }

    static signIn = (email, password) => {
        return new Promise((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(result => {
                    resolve(result)
                })
                .catch(err => reject(err))
        })
    }

    static signOut = () => {
        return new Promise((resolve, reject) => {
            firebase.auth().signOut().then(() => {
                resolve(true)
            })
                .catch(err => reject(err))
        })
    }

    static resetPassword = (email) => {
        return new Promise((resolve, reject) => {
            firebase.auth().sendPasswordResetEmail(email).then(() => {
                resolve(true)
            })
                .catch(err => reject(err))
        })
    }
}