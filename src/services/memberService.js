import firebase from '../utils/firebaseUtils'

export default class MemberService {
    static getById = (teamId, memberId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers').doc(memberId)
                .get()
                .then(snap => {
                    if (snap.exists) {
                        const doc = { id: snap.id, ...snap.data() }
                        return resolve(doc)
                    }
                    resolve(null)
                })
                .catch(err => reject(err))
        })
    }

    static getAll = (teamId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers')
                .where('deletedAt', '==', null)
                .orderBy('name').get()
                .then(snapshot => {
                    let memberList = []
                    snapshot.forEach(doc => memberList.push({ id: doc.id, ...doc.data() }))
                    resolve(memberList)
                })
                .catch(err => reject(err))
        })
    }

    static add = (teamId, memberId, name, emotion, attention, admin) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers').doc(memberId)
                .set({
                    teamId,
                    admin,
                    name,
                    emotion,
                    attention,
                    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
                    deletedAt: null
                }, { merge: true })
                .then(() => {
                    const doc = {
                        memberIds: firebase.firestore.FieldValue.arrayUnion(memberId)
                    }

                    if (admin) {
                        doc.adminIds = firebase.firestore.FieldValue.arrayUnion(memberId)
                    }

                    return firebase.firestore()
                        .collection('teams')
                        .doc(teamId)
                        .update(doc)
                        .then(() => resolve(true))
                })
                .catch(err => reject(err))
        })
    }

    static update = (teamId, memberId, data) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers')
                .doc(memberId)
                .update({
                    ...data,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => resolve(true))
                .catch(err => reject(err))
        })
    }

    static undoDelete = (teamId, memberId, name) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers')
                .doc(memberId)
                .update({
                    name,
                    deletedAt: null,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    const doc = {
                        memberIds: firebase.firestore.FieldValue.arrayUnion(memberId)
                    }

                    return firebase.firestore()
                        .collection('teams')
                        .doc(teamId)
                        .update(doc)
                        .then(() => resolve(true))
                })
                .catch(err => reject(err))
        })
    }

    static delete = (teamId, memberId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore()
                .collection('teams').doc(teamId)
                .collection('teamMembers')
                .doc(memberId)
                .update({
                    admin: false,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
                    deletedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    const doc = {
                        memberIds: firebase.firestore.FieldValue.arrayRemove(memberId),
                        adminIds: firebase.firestore.FieldValue.arrayRemove(memberId)
                    }
                    return firebase.firestore()
                        .collection('teams')
                        .doc(teamId)
                        .update(doc)
                        .then(() => resolve(true))
                })
                .catch(err => reject(err))
        })
    }
}