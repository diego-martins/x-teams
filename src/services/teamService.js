import firebase from '../utils/firebaseUtils'

export default class TeamService {
    static getById = (teamId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('teams')
                .where(firebase.firestore.FieldPath.documentId(), '==', teamId)
                .where('deletedAt', '==', null).get()
                .then(snapshot => {
                    const list = []
                    snapshot.forEach(doc => list.push({ id: doc.id, ...doc.data() }))
                    resolve(list[0] || null)
                })
                .catch(err => reject(err))
        })
    }

    static getAll = (userId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('teams')
                .where('memberIds', 'array-contains', userId)
                .where('deletedAt', '==', null)
                .orderBy('lowerName').get()
                .then(snapshot => {
                    const list = []
                    snapshot.forEach(doc => list.push({ id: doc.id, ...doc.data() }))
                    resolve(list)
                })
                .catch(err => reject(err))
        })
    }

    static create = (userId, name, company) => {
        return new Promise((resolve, reject) => {
            let docRef = firebase.firestore().collection(`teams`).doc();
            docRef.set({
                userId,
                name,
                company,
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                modifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
                deletedAt: null
            })
                .then(() => {
                    resolve({ id: docRef.id })
                })
                .catch(err => reject(err))
        })
    }

    static update = (id, name, company) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('teams').doc(id)
                .update({
                    name,
                    company,
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    resolve(true)
                })
                .catch(err => reject(err))
        })
    }

    static delete = (id) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection(`teams`).doc(id)
                .update({
                    deletedAt: firebase.firestore.FieldValue.serverTimestamp()
                })
                .then(() => {
                    resolve(true)
                })
                .catch(err => reject(err))
        })
    }

    static existsBy = (userId, name, company, exceptId) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('teams')
                .where('userId', '==', userId)
                .where('lowerName', '==', name)
                .where('lowerCompany', '==', company)
                .get()
                .then((snapshot) => {
                    const items = []
                    snapshot.forEach(doc => items.push({ id: doc.id, ...doc.data() }))
                    resolve(items.filter(item => !exceptId || item.id !== exceptId).length > 0)
                })
                .catch(err => reject(err))
        })
    }
}