import firebase from '../utils/firebaseUtils'

export default class UserService {
    static getMe = () => {
        return new Promise((resolve, reject) => {
            const id = firebase.auth().currentUser.uid
            firebase.firestore().collection('users').doc(id).get()
                .then(snapshot => {
                    const data = snapshot.data()
                    if (!data) {
                        resolve(null)
                        return
                    }
                    resolve({ id: snapshot.id, ...data })
                })
                .catch(err => reject(err))
        })
    }

    static get = (id) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('users').doc(id).get()
                .then(snapshot => {
                    const data = snapshot.data()
                    if (!data) {
                        resolve(null)
                        return
                    }
                    resolve({ id: snapshot.id, ...data })
                })
                .catch(err => reject(err))
        })
    }

    static create = (userId, firstName, lastName, email, emotion, attention) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('users').doc(userId)
                .set({
                    firstName,
                    lastName,
                    email,
                    emotion,
                    attention,
                    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                    modifiedAt: firebase.firestore.FieldValue.serverTimestamp(),
                    deletedAt: null
                })
                .then(() => {
                    resolve(true)
                })
                .catch(err => reject(err))
        })
    }

    static getByEmail = (email) => {
        return new Promise((resolve, reject) => {
            firebase.firestore().collection('users')
                .where('email', '==', email).get()
                .then((snapshot) => {
                    const items = []
                    snapshot.forEach(doc => items.push({ id: doc.id, ...doc.data() }))
                    resolve(items[0] || null)
                })
                .catch(err => reject(err))
        })
    }

    static updateFCMToken = (fcmToken) => {
        return new Promise((resolve, reject) => {
            const id = firebase.auth().currentUser.uid
            firebase.firestore().collection('users').doc(id)
                .set({ fcmToken }, { merge: true })
        })
    }
}