import { createMuiTheme } from '@material-ui/core/styles'
import { blue, indigo, red } from '@material-ui/core/colors'

export default createMuiTheme({
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary: {
            main: blue[500],
            contrastText: '#fff'
        },
        secondary: indigo,
        error: red
    }
})