const _firebaseConfigDev = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: ""
}

const _publicVapidKeyDev = ''

const _firebaseConfigProd = {
    apiKey: "",
    authDomain: "",
    databaseURL: ",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "",
    measurementId: ""
}

const _publicVapidKeyProd = ''

const getFirebaseConfig = (env) => {
    if (env === 'prod') {
        return _firebaseConfigProd
    }

    return _firebaseConfigDev
}

const getPublicVapidKey = (env) => {
    if (env === 'prod') {
        return _publicVapidKeyProd
    }

    return _publicVapidKeyDev
}

export const firebaseConfig = getFirebaseConfig(process.env.REACT_APP_ENV)
export const publicVapidKey = getPublicVapidKey(process.env.REACT_APP_ENV)
