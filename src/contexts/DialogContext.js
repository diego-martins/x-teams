import React from 'react'

export const DialogContext = React.createContext({
    isShow: false,
    props: {},
    showDialog: () => { },
    hideDialog: () => { }
})

export class DialogProvider extends React.Component {

    showDialog = (props = {}) => {
        if (!props.onOk) { props.onOk = () => this.hideDialog() }
        this.setState({ isShow: true, props })
    }

    hideDialog = () => this.setState({ isShow: false, props: {} })

    state = {
        isShow: false,
        props: {},
        showDialog: this.showDialog,
        hideDialog: this.hideDialog
    }

    render() {
        return (
            <DialogContext.Provider value={this.state}>
                {this.props.children}
            </DialogContext.Provider>
        );
    }
}

export const DialogConsumer = DialogContext.Consumer