import emotionAngry from '../assets/images/emotion_angry.png'
import emotionAngryRed from '../assets/images/emotion_angry_red.png'
import emotionAngryGreen from '../assets/images/emotion_angry_green.png'
import emotionAngryYellow from '../assets/images/emotion_angry_yellow.png'
import emotionBad from '../assets/images/emotion_bad.png'
import emotionBadRed from '../assets/images/emotion_bad_red.png'
import emotionBadGreen from '../assets/images/emotion_bad_green.png'
import emotionBadYellow from '../assets/images/emotion_bad_yellow.png'
import emotionOk from '../assets/images/emotion_ok.png'
import emotionOkRed from '../assets/images/emotion_ok_red.png'
import emotionOkGreen from '../assets/images/emotion_ok_green.png'
import emotionOkYellow from '../assets/images/emotion_ok_yellow.png'
import emotionGood from '../assets/images/emotion_good.png'
import emotionGoodRed from '../assets/images/emotion_good_red.png'
import emotionGoodGreen from '../assets/images/emotion_good_green.png'
import emotionGoodYellow from '../assets/images/emotion_good_yellow.png'
import emotionHappy from '../assets/images/emotion_happy.png'
import emotionHappyRed from '../assets/images/emotion_happy_red.png'
import emotionHappyGreen from '../assets/images/emotion_happy_green.png'
import emotionHappyYellow from '../assets/images/emotion_happy_yellow.png'
import * as constants from '../constants'

const emotions = [
    {
        id: constants.EMOTION_ANGRY,
        name: "angry"
    },
    {
        id: constants.EMOTION_BAD,
        name: "bad"
    },
    {
        id: constants.EMOTION_OK,
        name: "ok"
    },
    {
        id: constants.EMOTION_GOOD,
        name: "good"
    },
    {
        id: constants.EMOTION_HAPPY,
        name: "happy"
    }
]

const attentions = [
    {
        id: constants.ATTENTION_GREEN,
        attention: constants.ATTENTION_GREEN,
        name: "available"
    },
    {
        id: constants.ATTENTION_YELLOW,
        attention: constants.ATTENTION_YELLOW,
        name: "busy"
    },
    {
        id: constants.ATTENTION_RED,
        attention: constants.ATTENTION_RED,
        name: "unavailable"
    }
]

const _getEmotion = (id) => emotions.find(item => item.id === id)
const _getAttention = (id) => attentions.find(item => item.id === id)

const _getSummary = (t, emotion, attention) => {
    const emotionText = _getEmotion(emotion)
    const attentionText = _getAttention(attention)

    if (emotionText && !attentionText) return t(`emotion_${emotionText.name}`)
    if (!emotionText && attentionText) return t(`attention_${attentionText.name}`)
    if (!emotionText && !attentionText) return ''

    let emotionName = t(`emotion_${emotionText.name}`)
    let attentionName = t(`attention_${attentionText.name}`)
    let union = t('union_and')

    if ((emotion >= constants.EMOTION_OK && attention < constants.ATTENTION_GREEN)
        || (attention === constants.ATTENTION_GREEN && emotion < constants.EMOTION_OK)) {
        union = t('union_but')
    }

    return `${emotionName} ${union} ${attentionName}`
}

const _getMemberSummary = (t, member) => {
    let message = member.admin ? 'admin • ' : ''
    return message + _getSummary(t, member.emotion, member.attention)
}

const _getAttentionStyleColor = (attentionId) => {
    switch (attentionId) {
        case constants.ATTENTION_RED: return {
            color: '#000',
            backgroundColor: '#F5291A'
        }
        case constants.ATTENTION_YELLOW: return {
            color: '#000',
            backgroundColor: '#FFC10E'
        }
        case constants.ATTENTION_GREEN: return {
            color: '#000',
            backgroundColor: '#4caf50'
        }
        default: return {}
    }
}

const _getEmotionIcon = (emotionId, attentionId) => {
    switch (emotionId) {
        case constants.EMOTION_ANGRY:
            switch (attentionId) {
                case constants.ATTENTION_RED: return emotionAngryRed
                case constants.ATTENTION_GREEN: return emotionAngryGreen
                case constants.ATTENTION_YELLOW: return emotionAngryYellow
                default: return emotionAngry
            }
        case constants.EMOTION_BAD:
            switch (attentionId) {
                case constants.ATTENTION_RED: return emotionBadRed
                case constants.ATTENTION_GREEN: return emotionBadGreen
                case constants.ATTENTION_YELLOW: return emotionBadYellow
                default: return emotionBad
            }
        case constants.EMOTION_OK:
            switch (attentionId) {
                case constants.ATTENTION_RED: return emotionOkRed
                case constants.ATTENTION_GREEN: return emotionOkGreen
                case constants.ATTENTION_YELLOW: return emotionOkYellow
                default: return emotionOk
            }
        case constants.EMOTION_GOOD:
            switch (attentionId) {
                case constants.ATTENTION_RED: return emotionGoodRed
                case constants.ATTENTION_GREEN: return emotionGoodGreen
                case constants.ATTENTION_YELLOW: return emotionGoodYellow
                default: return emotionGood
            }
        case constants.EMOTION_HAPPY:
            switch (attentionId) {
                case constants.ATTENTION_RED: return emotionHappyRed
                case constants.ATTENTION_GREEN: return emotionHappyGreen
                case constants.ATTENTION_YELLOW: return emotionHappyYellow
                default: return emotionHappy
            }
        default: return null
    }
}

export const getEmotions = () => emotions
export const getAttentions = () => attentions
export const getEmotion = _getEmotion
export const getAttention = _getAttention
export const getSummary = _getSummary
export const getMemberSummary = _getMemberSummary
export const getEmotionIcon = _getEmotionIcon
export const getAttentionStyleColor = _getAttentionStyleColor
