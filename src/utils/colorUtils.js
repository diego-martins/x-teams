export const stringToColor = (str, s = 50, l = 60) => {
    let hash = 0
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash)
    }

    let h = hash % 360
    return {
        back: 'hsl(' + h + ', ' + s + '%, ' + l + '%)',
        front: l > 70 ? '#555' : '#fff'
    }
}

export const hexToHSL = (str, moreS = 0, moreL = 0) => {
    // Convert hex to RGB first
    let r = 0, g = 0, b = 0
    if (str.length === 4) {
        r = "0x" + str[1] + str[1]
        g = "0x" + str[2] + str[2]
        b = "0x" + str[3] + str[3]
    } else if (str.length === 7) {
        r = "0x" + str[1] + str[2]
        g = "0x" + str[3] + str[4]
        b = "0x" + str[5] + str[6]
    }
    // Then to HSL
    r /= 255
    g /= 255
    b /= 255
    let cmin = Math.min(r, g, b),
        cmax = Math.max(r, g, b),
        delta = cmax - cmin,
        h = 0,
        s = 0,
        l = 0

    if (delta === 0)
        h = 0
    else if (cmax === r)
        h = ((g - b) / delta) % 6
    else if (cmax === g)
        h = (b - r) / delta + 2
    else
        h = (r - g) / delta + 4

    h = Math.round(h * 60)

    if (h < 0)
        h += 360

    l = (cmax + cmin) / 2
    s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1))
    s = +(s * 100).toFixed(1)
    l = +(l * 100).toFixed(1)

    return "hsl(" + h + "," + (s + moreS) + "%," + (l + moreL) + "%)"
}