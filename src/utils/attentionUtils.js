import * as constants from '../constants'

export const getAttentionStyleColor = (attention) => {
    switch (attention) {
        case constants.ATTENTION_RED: return {
            color: '#000',
            backgroundColor: '#F5291A'
        }
        case constants.ATTENTION_YELLOW: return {
            color: '#000',
            backgroundColor: '#FFC10E'
        }
        case constants.ATTENTION_GREEN: return {
            color: '#000',
            backgroundColor: '#4caf50'
        }
        default: return {}
    }
}