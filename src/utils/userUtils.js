const _getInitialUserName = (name) => {
    if (!name) return ''
    const parts = name.split(' ')
    const firstPart = parts[0] || ''
    if (parts.length === 1) {
        return firstPart[0].toUpperCase()
    }
    const lastPart = parts[parts.length - 1] || ''
    return `${firstPart[0] || ''}${lastPart[0] || ''}`.toUpperCase()
}

export const getInitialUserName = _getInitialUserName
