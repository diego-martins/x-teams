import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/messaging'
import { firebaseConfig, publicVapidKey } from '../config/firebase'

export const initializeApp = () => {
    firebase.initializeApp(firebaseConfig)
}

export const initializeMessaging = () => {
    firebase.messaging().usePublicVapidKey(publicVapidKey)
}

export const requestPermission = () => {
    return new Promise((resolve, reject) => {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                console.info('Notification permission granted.')
                firebase.messaging().getToken()
                    .then(token => resolve(token))
                    .catch(err => reject(err))
            } else {
                console.error('Unable to get permission to notify.')
                reject(new Error('Unable to get permission to notify.'))
            }
        })
    })
}

export const tsToMillis = (timestamp) => {
    const ts = new firebase.firestore.Timestamp(timestamp.seconds, timestamp.nanoseconds)
    return ts.toMillis()
}

export default firebase