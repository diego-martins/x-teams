importScripts('/service-worker.js')

if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`)

    workbox.routing.registerRoute(
        /\.json$/,
        new workbox.strategies.StaleWhileRevalidate({
            // Use a custom cache name.
            cacheName: 'json-cache',
        })
    );

} else {
    console.log(`Boo! Workbox didn't load 😬`)
}

const _update = () => {
    console.log('update0')
    console.log('update1')
    console.log('update1')
    console.log('update1')
    console.log('update1')
    console.log('update1')
}

/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
// importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js")
// importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js")

// firebase.initializeApp({
//     messagingSenderId: "199213454360"
// })

// const messaging = firebase.messaging()

// messaging.setBackgroundMessageHandler((payload) => {
//     console.log('[sw.js] Received background message ', payload)
//     // Customize notification here
//     const notificationTitle = payload.title
//     const notificationOptions = {
//         body: payload.body,
//         icon: '/icon.png'
//     }

//     return self.registration.showNotification(notificationTitle,
//         notificationOptions)
// })

// self.addEventListener('notificationclick', (event) => {
//     const clickedNotification = event.notification;
//     clickedNotification.close()
//     const promiseChain = clients
//         .matchAll({
//             type: 'window',
//             includeUncontrolled: true
//         })
//         .then(windowClients => {
//             let matchingClient = null;
//             for (let i = 0; i < windowClients.length; i++) {
//                 const windowClient = windowClients[i];
//                 if (windowClient.url === feClickAction) {
//                     matchingClient = windowClient
//                     break
//                 }
//             }
//             if (matchingClient) {
//                 return matchingClient.focus()
//             } else {
//                 return clients.openWindow(feClickAction)
//             }
//         })
//     event.waitUntil(promiseChain)
// })